import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

Dialog {
    id: weatherSettingsDialog
    title: "Weather Settings"

    property var params: null
    property var locations: []

    OptionSelector {
        text: i18n.tr("Units")
        model: [i18n.tr("Celsius"),
                i18n.tr("Fahrenheit")]
        onDelegateClicked: {
            weatherUnit = selectedIndex
        }
    }

    Label {
        text: i18n.tr("Locations")
        font.bold: true
    }

    Label {
        width: parent.width
        text: "Not implemented yet"
    }

    Button {
        text: i18n.tr("OK")
        color: LomiriColors.green
        onClicked: {
            // Do something with index
            PopupUtils.close(weatherSettingsDialog)
        }
    }

    Button {
        text: i18n.tr("Cancel")
        color: LomiriColors.red
        onClicked: PopupUtils.close(weatherSettingsDialog)
    }
}
