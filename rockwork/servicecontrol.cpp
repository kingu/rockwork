#include "servicecontrol.h"

#include <QFile>
#include <QDir>
#include <QDebug>
#include <QCoreApplication>
#include <QProcess>

#define SERVICE_NAME "org.freedesktop.systemd1"
#define DBUS_PATH "/org/freedesktop/systemd1"
#define DBUS_INTERFACE "org.freedesktop.systemd1.Manager"

ServiceControl::ServiceControl(QObject *parent):
  QObject(parent),
  m_systemdInterface(SERVICE_NAME, DBUS_PATH, DBUS_INTERFACE, QDBusConnection::sessionBus())
{
    if (!m_systemdInterface.isValid())
        qDebug() << "Failed to connect to systemd bus!";
}

QString ServiceControl::serviceName() const
{
    return m_serviceName;
}

void ServiceControl::setServiceName(const QString &serviceName)
{
    if (m_serviceName != serviceName) {
        m_serviceName = serviceName;
        emit serviceNameChanged();
    }
}

bool ServiceControl::serviceFileInstalled() const
{
    if (m_serviceName.isEmpty()) {
        qDebug() << "Service name not set.";
        return false;
    }
    QFile f(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/systemd/user/" + m_serviceName + ".service");
    return f.exists();
}

bool ServiceControl::installServiceFile()
{
    if (m_serviceName.isEmpty()) {
        qDebug() << "Service name not set. Cannot generate service file.";
        return false;
    }

    QFile f(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/systemd/user/" + m_serviceName + ".service");
    if (f.exists()) {
        qDebug() << "Service file already existing...";
        return false;
    }

    if (!QDir(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/systemd/user/ubuntu-touch-session.target.wants").exists()) {
	qDebug() << "creating systemd directory";
	QDir().mkpath(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/systemd/user/ubuntu-touch-session.target.wants");
    }

    if (!f.open(QFile::WriteOnly | QFile::Truncate)) {
        qDebug() << "Cannot create service file";
        return false;
    }

    QString appDir = qApp->applicationDirPath();
    // Try to replace version with "current" to be more robust against updates
    appDir.replace(QRegExp("rockwork.mzanetti\\/[0-9.-]*\\/"), "rockwork.mzanetti/current/");

    f.write("[Unit]\n");
    f.write("Description=rockworkd\n");
    f.write("[Service]\n");
    f.write("Environment=LD_LIBRARY_PATH=" + appDir.toUtf8() + "/../:$LD_LIBRARY_PATH\n");
    f.write("ExecStart=" + appDir.toUtf8() + "/" + m_serviceName.toUtf8() +"\n");
    f.write("[Install]\n");
    f.write("WantedBy=ubuntu-touch-session.target\n");
    f.close();

    // Create a link for systemd service to enable automatically
    QFile::link(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/systemd/user/" + m_serviceName + ".service",
                QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/systemd/user/ubuntu-touch-session.target.wants/" + m_serviceName + ".service");

    // Reload the systemd daemon
    m_systemdInterface.call("Reload");
    return true;
}

bool ServiceControl::removeServiceFile()
{
    if (m_serviceName.isEmpty()) {
        qDebug() << "Service name not set.";
        return false;
    }
    QFile f(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + "/systemd/user/" + m_serviceName + ".service");
    return f.remove();
}

bool ServiceControl::serviceRunning()
{
    QString status;
    QDBusReply<QDBusObjectPath> unitPath = m_systemdInterface.call("GetUnit", m_serviceName + ".service");

    QDBusInterface serviceInterface("org.freedesktop.systemd1",
                                    unitPath.value().path(),
                                    "org.freedesktop.DBus.Properties",
                                    QDBusConnection::sessionBus());

    QDBusReply<QDBusVariant> activeState = serviceInterface.call("Get", "org.freedesktop.systemd1.Unit", "ActiveState");
    status = activeState.value().variant().toString();

    if (status == "active") {
        return true;
    } else {
        return false;
    }
    // We shouldn't be here, but if we are, something went wrong...
    return false;
}

bool ServiceControl::setServiceRunning(bool running)
{
    if (running && !serviceRunning()) {
        startService();
        return true;
    } else if (!running && serviceRunning()) {
        stopService();
        return true;
    }
    return true; // Requested state is already the current state.
}

void ServiceControl::startService()
{
    m_systemdInterface.call("StartUnit", m_serviceName + ".service", "fail");
}

void ServiceControl::stopService()
{
    m_systemdInterface.call("StopUnit", m_serviceName + ".service", "fail");
}

void ServiceControl::restartService()
{
    m_systemdInterface.call("RestartUnit", m_serviceName + ".service", "fail");
}
