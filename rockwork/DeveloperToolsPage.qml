import QtQuick 2.4
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

Page {
    id: root
    anchors.fill: parent

    header: PageHeader {
        id: header
        title: i18n.tr("Developer Tools")
        flickable: pageFlickable
    }

    property var pebble: null

    //Creating the menu list this way to allow the text field to be translatable (http://askubuntu.com/a/476331)
    ListModel {
        id: devMenuModel
        dynamicRoles: true
    }

    Component.onCompleted: {
        populateDevMenu();
    }

    function populateDevMenu() {
        devMenuModel.clear();

        devMenuModel.append({
            icon: "camera-app-symbolic",
            text: i18n.tr("Screenshots"),
            page: "ScreenshotsPage.qml",
            dialog: "",
            color: "gold"
        });
        devMenuModel.append({
            icon: "stock_application",
            text: i18n.tr("Install app or watchface from file"),
            page: "ImportPackagePage.qml",
            dialog: null,
            color: LomiriColors.blue
        });
        devMenuModel.append({
            icon: "stock_application",
            text: i18n.tr("Install firmware from file"),
            page: "ImportFirmware.qml",
            dialog: null,
            color: LomiriColors.green
        });
    }

    Flickable {
        id: pageFlickable
        anchors.fill: parent

        ColumnLayout {
            anchors.fill: parent

            Repeater {
                id: menuRepeater
                model: devMenuModel
                delegate: ListItem {

                    RowLayout {
                        anchors.fill: parent
                        anchors.margins: units.gu(1)

                        LomiriShape {
                            Layout.fillHeight: true
                            Layout.preferredWidth: height
                            backgroundColor: model.color
                                Icon {
                                    anchors.fill: parent
                                    anchors.margins: units.gu(.5)
                                    name: model.icon
                                    color: "white"
                                }
                        }


                        Label {
                            text: model.text
                            Layout.fillWidth: true
                        }
                    }

                    onClicked: {
                        if (model.page) {
                            pageStack.push(Qt.resolvedUrl(model.page), {pebble: root.pebble})
                        }
                        if (model.dialog) {
                            PopupUtils.open(model.dialog)
                        }
                    }
                }
            }

            Item {
                Layout.fillHeight: true
                Layout.fillWidth: true
            }
        }
    }
}
