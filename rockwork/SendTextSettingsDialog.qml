import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

Dialog {
    id: sendTextSettingsDialog
    title: "Messaging Settings"

    property var params: null
    property var contacts: []

    Label {
        text: i18n.tr("Contacts")
        font.bold: true
    }

    Label {
        width: parent.width
        text: "Not implemented yet"
    }

    Label {
        text: i18n.tr("Messages")
        font.bold: true
    }

    Label {
        width: parent.width
        text: "Not implemented yet"
    }

    Button {
        text: i18n.tr("Cancel")
        color: LomiriColors.red
        onClicked: PopupUtils.close(sendTextSettingsDialog)
    }
}
