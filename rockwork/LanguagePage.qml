import QtQuick 2.4
import Lomiri.Components 1.3

Page {
    id: root
    property var pebble: null
    property var languages: []
    property int currentLanguage
    onPebbleChanged: if(pebble) loadLangs()

    Connections {
        target: pebble
        onLanguageVersionChanged: {
            loadLangs();
        }
    }

    header: PageHeader {
        title: i18n.tr("Language Settings")
        flickable: flicker
    }

    Flickable {
        id: flicker
        anchors.fill: parent
        topMargin: root.header.flickable ? 0 : root.header.height
        contentHeight: content.height

        Column {
            id: content
            width: parent.width

            Label {
                text: i18n.tr("Select Language")
            }

            OptionSelector {
                id: langSel
                width: parent.width
                model: languages.map(function(l){return l.localName;})
                selectedIndex: currentLanguage
                onDelegateClicked: {
                    pebble.loadLanguagePack(languages[index].file);
                }
            }
        }
    }

    function loadLangs() {
        if(languages.length>0) {
            currentLanguage = currentLang();
            return;
        }
        var re = /(\d+)\.(\d+)\.?(\d+)?/;
        var ret = re.exec(pebble.softwareVersion);
        var ver = "3.8.0";
        if(ret) {
            ver = ret[1]+"."+ret[2]+"."+(ret[3]? ret[3] : "0");
        }
        var url = "https://lp.rebble.io/v1/languages?hw="+pebble.platformString+"&firmware="+ver;
        var xhr = new XMLHttpRequest();
        xhr.open("GET",url);
        xhr.onreadystatechange = function() {
            if(xhr.readyState == xhr.DONE) {
                if(xhr.status == 200) {
                    var json = JSON.parse(xhr.responseText);
                    if("languages" in json) {
                        languages = json.languages;
                        currentLanguage = currentLang();
                        return;
                    }
                }
                console.log("XHR Error:",xhr.status,xhr.responseText);
            }
        };
        console.log("Fetching langugages from",url);
        xhr.send();
    }

    function currentLang() {
        var currentlocale = pebble.languageVersion.split(":")[0];
        for (var i = 0; i < languages.length; i++) {
            if (languages[i].ISOLocal == currentlocale) {
                currentLanguage = i;
                return;
            }
        }
    }
}

