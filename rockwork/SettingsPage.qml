import QtQuick 2.4
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3

Page {
    id: root

    property var pebble: null

    header: PageHeader {
        title: i18n.tr("Settings")
        flickable: flickable
    }

    Flickable {
        id: flickable
        topMargin: root.header.flickable ? 0 : root.header.height
        contentHeight: contentLayout.height
        anchors.fill: parent

        ColumnLayout {
            id: contentLayout
            anchors.fill: parent
            anchors.margins: units.gu(1)
            spacing: units.gu(1)

            Label {
                Layout.fillWidth: true
                text: i18n.tr("Distance Units")
                font.bold: true
            }

            RowLayout {
                Layout.fillWidth: true
                CheckBox {
                    id: metricUnitsCheckbox
                    checked: !root.pebble.imperialUnits
                    onClicked: {
                        checked = true
                        root.pebble.imperialUnits = false;
                        imperialUnitsCheckBox.checked = false;
                    }
                }
                Label {
                    text: i18n.tr("Metric")
                    Layout.fillWidth: true
                }
                CheckBox {
                    id: imperialUnitsCheckBox
                    checked: root.pebble.imperialUnits
                    onClicked: {
                        checked = true
                        root.pebble.imperialUnits = true;
                        metricUnitsCheckbox.checked = false;
                    }
                }
                Label {
                    text: i18n.tr("Imperial")
                    Layout.fillWidth: true
                }
            }
            Divider {}

            Label {
                text: i18n.tr("Language Settings")
                Layout.fillWidth: true
                font.bold: true
            }
            Button {
                width: parent.width
                text: i18n.tr("Change Language")
                onClicked: pageStack.push(Qt.resolvedUrl("LanguagePage.qml"), {pebble: pebble})
            }
            Divider {}

            Label {
                text: i18n.tr("Voice Settings")
                Layout.fillWidth: true
                font.bold: true
            }
            Button {
                width: parent.width
                text: i18n.tr("Change Voice Settings")
                onClicked: pageStack.push(Qt.resolvedUrl("VoiceSettingsPage.qml"), {pebble: pebble})
            }
            Divider {}

            Label {
                text: i18n.tr("Timeline")
                Layout.fillWidth: true
                font.bold: true
            }
            RowLayout {
                Layout.fillWidth: true
                Label {
                    text: i18n.tr("Sync calendar to timeline")
                    Layout.fillWidth: true
                }
                Switch {
                    checked: root.pebble.calendarSyncEnabled
                    onClicked: {
                        root.pebble.calendarSyncEnabled = checked;
                    }
                }
            }

            RowLayout {
                Layout.fillWidth: true
                Label {
                    text: i18n.tr("Sync Apps from Cloud")
                    Layout.fillWidth: true
                }
                Switch {
                    checked: root.pebble.syncAppsFromCloud
                    onClicked: root.pebble.syncAppsFromCloud = checked
                }
            }
            Button {
                width: parent.width
                text: i18n.tr("Reset Timeline")
                onClicked: pebble.resetTimeline()
            }

            Divider {}

            ColumnLayout {
                Label {
                    text: i18n.tr("Timeline Window Start (days ago)")
                    Layout.fillWidth: true
                }
                TextField {
                    width: parent.width
                    inputMethodHints: Qt.ImhDigitsOnly
                    text: pebble.timelineWindowStart
                    onAccepted: {
                        pebble.timelineWindowStart=text
                        pebble.setTimelineWindow()
                    }
                }

                Label {
                    text: i18n.tr("Timeline Window End (days ahead)")
                    Layout.fillWidth: true
                }
                TextField {
                    width: parent.width
                    inputMethodHints: Qt.ImhDigitsOnly
                    text: pebble.timelineWindowEnd
                    onAccepted: {
                        pebble.timelineWindowEnd=text
                        pebble.setTimelineWindow()
                    }
                }

                Label {
                    text: i18n.tr("Notification re-delivery expiration (seconds)")
                    Layout.fillWidth: true
                }
                TextField {
                    width: parent.width
                    placeholderText: i18n.tr("Notification re-delivery expiration (seconds)")
                    inputMethodHints: Qt.ImhDigitsOnly
                    text: pebble.timelineWindowFade
                    onAccepted: {
                        pebble.timelineWindowFade=text
                        pebble.setTimelineWindow()
                    }
                }

                Button {
                    width: parent.width
                    text: i18n.tr("Set Timeline Window")
                    onClicked: pebble.setTimelineWindow()
                }
            }
            Divider {}

            Label {
                text: i18n.tr("Active Timeline WebSync account")
                Layout.fillWidth: true
                font.bold: true
            }
            ColumnLayout {
                Layout.fillWidth: true

                Label {
                    width: parent.width
                    text: "Name: " + pebble.accountName
                    visible: pebble.accountName !== ""
                }

                Label {
                    width: parent.width
                    text: "Email: " + pebble.accountEmail
                    visible: pebble.accountEmail !== ""
                }

                Button {
                    width: parent.width
                    text: pebble.loggedIn ? i18n.tr("Logout") : i18n.tr("Login")
                    onClicked: if (pebble.loggedIn) {
                                   pebble.setupWebServices("");
                                   loggedIn = "";
                               } else {
                                   pageStack.push(Qt.resolvedUrl("AppSettingsPage.qml"), {
                                                  url: "https://boot.rebble.io",
                                                  pebble: pebble
                                              })
                               }
                }
            }

            Divider {}

            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true
            }
        }
    }
}
