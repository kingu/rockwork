import QtQuick 2.4
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import RockWork 1.0

Page {
    id: root

    property var pebble: null
    property bool showWatchApps: false
    property bool showWatchFaces: false
    property var model: showWatchApps ? pebble.installedApps : pebble.installedWatchfaces

    AppStoreClient {
        id: client
        hardwarePlatform: pebble.hardwarePlatform
        model: root.model
    }

    header: PageHeader {
        title: showWatchApps ? (showWatchFaces ? i18n.tr("Apps & Watchfaces") : i18n.tr("Apps")) : i18n.tr("Watchfaces")

        trailingActionBar {
            actions: [
                Action {
                    iconName: "add"
                    onTriggered: pageStack.push(Qt.resolvedUrl("AppStorePage.qml"), {pebble: root.pebble, showWatchApps: root.showWatchApps, showWatchFaces: root.showWatchFaces})
                }
            ]
        }
    }

    function configureApp(uuid) {
        var popup = null;
        var cbacc = null;
        if (uuid == "{36d8c6ed-4c83-4fa1-a9e2-8f12dc941f8c}") {
            popup = PopupUtils.open(Qt.resolvedUrl("HealthSettingsDialog.qml"), root, {healthParams: pebble.healthParams});
            cbacc = function() {
                pebble.healthParams = popup.healthParams
            };
        } else if(uuid === "{61b22bc8-1e29-460d-a236-3fe409a439ff}") {
            popup = PopupUtils.open(Qt.resolvedUrl("WeatherSettingsDialog.qml"), root, {
                                            params: pebble.weatherParams
                                        });
            cbacc = function () {
                pebble.weatherParams = popup.params
            };
        } else if(uuid === "{0863fc6a-66c5-4f62-ab8a-82ed00a98b5d}") {
            popup = PopupUtils.open(Qt.resolvedUrl("SendTextSettingsDialog.qml"), root, {
                                            params: pebble.sendTextParams
                                        });
            cbacc = function () {
                pebble.sendTextParams = popup.params
            };
        }
        if(popup && cbacc) {
            popup.accepted.connect(cbacc);
        } else {
            pebble.requestConfigurationURL(uuid);
        }
    }

    Item {
        anchors { left: parent.left; right: parent.right; top: header.bottom; bottom: parent.bottom }
        ListView {
            id: listView
            anchors.fill: parent
            model: root.model
            clip: true
            property real realContentY: contentY + originY

            delegate: InstalledAppDelegate {
                id: delegate
                uuid: model.uuid
                name: model.name
                iconSource: model.icon
                vendor: model.vendor
                version: model.version
                candidate: (model.latest && model.latest !== model.version) ? model.latest : ""
                visible: dndArea.draggedIndex !== index
                hasGrip: index > 0
                isSystemApp: model.isSystemApp
                hasSettings: model.hasSettings

                onDeleteApp: {
                    pebble.removeApp(model.uuid)
                }
                onConfigureApp: {
                    root.configureApp(model.uuid)
                }

                Component.onCompleted: if(!model.isSystemApp && !model.latest) { client.fetchAppDetails(model.storeId) }
                onUpgrade: {
                    pageStack.push(Qt.resolvedUrl("AppUpgradePage.qml"), {
                                                  pebble: root.pebble,
                                                  app_model: root.model,
                                                  app_index: index
                                                  })
                }

                onClicked: {
                    PopupUtils.open(dialogComponent, root, {app: listView.model.get(index)})
                }
            }
        }
        MouseArea {
            id: dndArea
            anchors {
                top: parent.top
                bottom: parent.bottom
                right: parent.right
            }
            drag.axis: Drag.YAxis
            propagateComposedEvents: true
            width: units.gu(5)

            property int startY: 0
            property int draggedIndex: -1


            onPressAndHold: {
                startY = mouseY;
                draggedIndex = Math.floor((listView.realContentY + mouseY) / fakeDragItem.height)
                if (draggedIndex == 0) {
                    print("cannot drag settings app");
                    return;
                }

                var draggedItem = listView.model.get(draggedIndex);
                fakeDragItem.uuid = draggedItem.uuid;
                fakeDragItem.name = draggedItem.name;
                fakeDragItem.vendor = draggedItem.vendor;
                fakeDragItem.iconSource = draggedItem.icon;
                fakeDragItem.isSystemApp = draggedItem.isSystemApp;
                fakeDragItem.y = (fakeDragItem.height * draggedIndex) - listView.realContentY
                drag.target = fakeDragItem;
            }

            onMouseYChanged: {
                var newIndex = Math.floor((listView.realContentY + mouseY) / fakeDragItem.height)

                if (newIndex > draggedIndex) {
                    newIndex = draggedIndex + 1;
                } else if (newIndex < draggedIndex) {
                    newIndex = draggedIndex - 1;
                } else {
                    return;
                }

                if (newIndex >= 1 && newIndex < listView.count) {
                    listView.model.move(draggedIndex, newIndex);
                    draggedIndex = newIndex;
                }
            }

            onReleased: {
                if (draggedIndex > -1) {
                    listView.model.commitMove();
                    draggedIndex = -1;
                    drag.target = null;
                }
            }
        }
    }



    InstalledAppDelegate {
        id: fakeDragItem
        visible: dndArea.draggedIndex != -1

    }

    Component {
        id: dialogComponent
        Dialog {
            id: dialog
            property var app: null

            RowLayout {
                SystemAppIcon {
                    height: titleCol.height
                    width: height
                    isSystemApp: app.isSystemApp
                    uuid: app.uuid
                    iconSource: app.icon
                }

                ColumnLayout {
                    id: titleCol
                    Layout.fillWidth: true

                    Label {
                        Layout.fillWidth: true
                        text: app.name
                        fontSize: "large"
                    }
                    Label {
                        Layout.fillWidth: true
                        text: app.vendor
                    }
                }
            }

            Button {
                text: i18n.tr("Launch")
                color: LomiriColors.green
                visible: app.uuid != "{36d8c6ed-4c83-4fa1-a9e2-8f12dc941f8c}"
                onClicked: {
                    pebble.launchApp(app.uuid);
                    PopupUtils.close(dialog);
                }
            }

            Button {
                text: i18n.tr("Configure")
                color: LomiriColors.blue
                visible: app.hasSettings
                onClicked: {
                    root.configureApp(app.uuid);
                    PopupUtils.close(dialog);
                }
            }

            Button {
                text: i18n.tr("Delete")
                color: LomiriColors.red
                visible: !app.isSystemApp
                onClicked: {
                    pebble.removeApp(app.uuid);
                    PopupUtils.close(dialog);
                }
            }

            Button {
                text: i18n.tr("Close")
                onClicked: PopupUtils.close(dialog)
            }
        }
    }
}
