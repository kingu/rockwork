import QtQuick 2.4
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3

Page {
    id: root
    property var pebble: null
    property var languages: []

    header: PageHeader {
        title: i18n.tr("Voice Settings")
        flickable: flickable
    }

    Flickable {
        id: flickable
        topMargin: root.header.flickable ? 0 : root.header.height
        contentHeight: contentLayout.height
        anchors.fill: parent

        ColumnLayout {
            id: contentLayout
            anchors.fill: parent
            anchors.margins: units.gu(1)
            spacing: units.gu(1)

            RowLayout {
                Layout.fillWidth: true
                Label {
                    text: i18n.tr("Enable voice transcription")
                    Layout.fillWidth: true
                }
                Switch {
                    checked: root.pebble.voiceDictationEnabled
                    onClicked: root.pebble.voiceDictationEnabled = checked
                }
            }

            Divider {}

            Label {
                text: i18n.tr("Languages")
                Layout.fillWidth: true
                font.bold: true
            }
            OptionSelector {
                width: parent.width
                Layout.fillWidth: true
                model: parseLanguageNames(root.pebble.voiceDictationLanguages())
                selectedIndex: root.pebble.voiceDictationLanguages().indexOf(root.pebble.voiceDictationLanguage)
                onDelegateClicked: {
                    root.pebble.voiceDictationLanguage = root.pebble.voiceDictationLanguages()[index];
                }
                visible: root.pebble.voiceDictationEnabled && root.pebble.loggedIn
            }
            Label {
                text: i18n.tr("Languages unavailable. Are you signed in?")
                Layout.fillWidth: true
                wrapMode: Text.WordWrap
                visible: !root.pebble.loggedIn
            }

            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true
            }
        }
    }
    function parseLanguageNames(locales) {
        var languages = [];
        for (var i = 0; i < locales.length; i++) {
            languages.push(Qt.locale(locales[i]).nativeLanguageName);
        }
        return languages;
    }
}
