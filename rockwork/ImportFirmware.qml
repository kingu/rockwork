import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Content 1.3

Page {
    id: root
    title: i18n.tr("Install firmware")

    property var pebble: null

    ContentPeerPicker {
        anchors.fill: parent
        handler: ContentHandler.Source
        contentType: ContentType.All
        showTitle: false

        onPeerSelected: {
            var transfer = peer.request();

            transfer.stateChanged.connect(function() {
                if (transfer.state == ContentTransfer.Charged) {
                    print("sideloading firmware", transfer.items[0].url)
                    root.pebble.sideloadFirmware(transfer.items[0].url)
                    pageStack.pop();
                }
            })
        }
    }
}

