#include "pebble.h"
#include "notificationsourcemodel.h"
#include "applicationsmodel.h"
#include "screenshotmodel.h"

#include <QDBusArgument>
#include <QDBusReply>
#include <QDebug>

// TODO: Bootstrapping config from
// https://boot.getpebble.com/api/config/android/v3/1055?locale=de_DE&app_version=3.13.0-1055-06644a6
Pebble::Pebble(const QDBusObjectPath &path, QObject *parent) : QObject(parent),
                                                               m_path(path)
{
    m_iface = new QDBusInterface("org.rockwork", path.path(), "org.rockwork.Pebble", QDBusConnection::sessionBus(), this);
    m_notifications = new NotificationSourceModel(this);
    m_installedApps = new ApplicationsModel(this);
    connect(m_installedApps, &ApplicationsModel::appsSorted, this, &Pebble::appsSorted);
    m_installedWatchfaces = new ApplicationsModel(this);
    m_screenshotModel = new ScreenshotModel(this);

    QDBusConnection::sessionBus().connect("org.rockwork", path.path(), "org.rockwork.Pebble", "Connected", this, SLOT(pebbleConnected()));
    QDBusConnection::sessionBus().connect("org.rockwork", path.path(), "org.rockwork.Pebble", "Disconnected", this, SLOT(pebbleDisconnected()));
    QDBusConnection::sessionBus().connect("org.rockwork", path.path(), "org.rockwork.Pebble", "InstalledAppsChanged", this, SLOT(refreshApps()));
    QDBusConnection::sessionBus().connect("org.rockwork", path.path(), "org.rockwork.Pebble", "OpenURL", this, SIGNAL(openURL(const QString &, const QString &)));
    QDBusConnection::sessionBus().connect("org.rockwork", path.path(), "org.rockwork.Pebble", "NotificationFilterChanged", this, SLOT(notificationFilterChanged(const QString &, const QString &, const QString &, const int)));
    QDBusConnection::sessionBus().connect("org.rockwork", path.path(), "org.rockwork.Pebble", "ScreenshotAdded", this, SLOT(screenshotAdded(const QString &)));
    QDBusConnection::sessionBus().connect("org.rockwork", path.path(), "org.rockwork.Pebble", "ScreenshotRemoved", this, SLOT(screenshotRemoved(const QString &)));
    QDBusConnection::sessionBus().connect("org.rockwork", path.path(), "org.rockwork.Pebble", "FirmwareUpgradeAvailableChanged", this, SLOT(refreshFirmwareUpdateInfo()));
    QDBusConnection::sessionBus().connect("org.rockwork", path.path(), "org.rockwork.Pebble", "LanguageVersionChanged", this, SIGNAL(languageVersionChanged()));
    QDBusConnection::sessionBus().connect("org.rockwork", path.path(), "org.rockwork.Pebble", "UpgradingFirmwareChanged", this, SIGNAL(refreshFirmwareUpdateInfo()));
    QDBusConnection::sessionBus().connect("org.rockwork", path.path(), "org.rockwork.Pebble", "LogsDumped", this, SIGNAL(logsDumped(bool)));
    QDBusConnection::sessionBus().connect("org.rockwork", path.path(), "org.rockwork.Pebble", "HealthParamsChanged", this, SIGNAL(healthParamsChanged()));
    QDBusConnection::sessionBus().connect("org.rockwork", path.path(), "org.rockwork.Pebble", "ImperialUnitsChanged", this, SIGNAL(imperialUnitsChanged()));
    QDBusConnection::sessionBus().connect("org.rockwork", path.path(), "org.rockwork.Pebble", "CalendarSyncEnabledChanged", this, SIGNAL(calendarSyncEnabledChanged()));
    QDBusConnection::sessionBus().connect("org.rockwork", path.path(), "org.rockwork.Pebble", "DevConnectionChanged", this, SLOT(devConStateChanged(bool)));
    QDBusConnection::sessionBus().connect("org.rockwork", path.path(), "org.rockwork.Pebble", "DevConnCloudChanged", this, SLOT(devConCloudChanged(bool)));
    QDBusConnection::sessionBus().connect("org.rockwork", path.path(), "org.rockwork.Pebble", "WebServicesUpdated", this, SIGNAL(webServicesUpdated()));
    QDBusConnection::sessionBus().connect("org.rockwork", path.path(), "org.rockwork.Pebble", "AccountInfoChanged", this, SIGNAL(accountInfoChanged()));
    QDBusConnection::sessionBus().connect("org.rockwork", path.path(), "org.rockwork.Pebble", "BatteryPercentageChanged", this, SIGNAL(batteryPercentageChanged()));

    dataChanged();
    refreshApps();
    refreshNotifications();
    refreshScreenshots();
    refreshFirmwareUpdateInfo();
}

bool Pebble::connected() const
{
    return m_connected;
}

QDBusObjectPath Pebble::path()
{
    return m_path;
}

QString Pebble::address() const
{
    return m_address;
}

QString Pebble::name() const
{
    return m_name;
}

QString Pebble::platformString() const
{
    return fetchProperty("PlatformString").toString();
}

QString Pebble::hardwarePlatform() const
{
    return m_hardwarePlatform;
}

QString Pebble::serialNumber() const
{
    return m_serialNumber;
}

QString Pebble::softwareVersion() const
{
    return m_softwareVersion;
}

QString Pebble::languageVersion() const
{
    return fetchProperty("LanguageVersion").toString();
}

void Pebble::loadLanguagePack(const QString &pblFile)
{
    qDebug() << "Requesting to load language from" << pblFile;
    m_iface->call("LoadLanguagePack", pblFile);
}

int Pebble::model() const
{
    return m_model;
}

bool Pebble::recovery() const
{
    return m_recovery;
}

bool Pebble::upgradingFirmware() const
{
    qDebug() << "upgrading firmware" << m_upgradingFirmware;
    return m_upgradingFirmware;
}

NotificationSourceModel *Pebble::notifications() const
{
    return m_notifications;
}

ApplicationsModel *Pebble::installedApps() const
{
    return m_installedApps;
}

ApplicationsModel *Pebble::installedWatchfaces() const
{
    return m_installedWatchfaces;
}

ScreenshotModel *Pebble::screenshots() const
{
    return m_screenshotModel;
}

bool Pebble::firmwareUpgradeAvailable() const
{
    return m_firmwareUpgradeAvailable;
}

QString Pebble::firmwareReleaseNotes() const
{
    return m_firmwareReleaseNotes;
}

QString Pebble::candidateVersion() const
{
    return m_candidateVersion;
}

QVariantMap Pebble::healthParams() const
{
    QDBusMessage m = m_iface->call("HealthParams");
    if (m.type() == QDBusMessage::ErrorMessage || m.arguments().count() == 0)
    {
        qWarning() << "Could not fetch health params" << m.errorMessage();
        return QVariantMap();
    }

    const QDBusArgument &arg = m.arguments().first().value<QDBusArgument>();

    QVariantMap mapEntryVariant;
    arg >> mapEntryVariant;

    qDebug() << "have health params" << mapEntryVariant;
    return mapEntryVariant;
}

void Pebble::setHealthParams(const QVariantMap &healthParams)
{
    m_iface->call("SetHealthParams", healthParams);
}

bool Pebble::imperialUnits() const
{
    return fetchProperty("ImperialUnits").toBool();
}

void Pebble::setImperialUnits(bool imperialUnits)
{
    qDebug() << "setting im units" << imperialUnits;
    m_iface->call("SetImperialUnits", imperialUnits);
}

bool Pebble::calendarSyncEnabled() const
{
    return fetchProperty("CalendarSyncEnabled").toBool();
}

void Pebble::setCalendarSyncEnabled(bool enabled)
{
    m_iface->call("SetCalendarSyncEnabled", enabled);
}

bool Pebble::devConnEnabled() const
{
    return fetchProperty("DevConnectionEnabled").toBool();
}
void Pebble::setDevConnEnabled(bool enabled)
{
    m_iface->call("SetDevConnEnabled", enabled);
}

bool Pebble::devConnCloudEnabled() const
{
    return fetchProperty("DevConnCloudEnabled").toBool();
}
void Pebble::setDevConnCloudEnabled(bool enabled)
{
    m_iface->call("SetDevConnCloudEnabled", enabled);
}

quint16 Pebble::devConListenPort() const
{
    return (quint16)fetchProperty("DevConnListenPort").toInt();
}
void Pebble::setDevConListenPort(quint16 port)
{
    m_iface->call("SetDevConnListenPort", port);
}

bool Pebble::devConnServerRunning() const
{
    return fetchProperty("DevConnectionState").toBool();
}

bool Pebble::devConCloudConnected() const
{
    return fetchProperty("DevConnCloudState").toBool();
}

void Pebble::devConStateChanged(bool state)
{
    qDebug() << "DevCon state hase changed:" << (state ? "running" : "stopped");
    emit devConnServerRunningChanged();
}

void Pebble::devConCloudChanged(bool state)
{
    qDebug() << "DevConCloud state changed:" << (state ? "connected" : "disconnected");
    emit devConCloudConnectedChanged();
}

QString Pebble::oauthToken() const
{
    return fetchProperty("oauthToken").toString();
}

bool Pebble::loggedIn() const
{
    return fetchProperty("loggedIn").toBool();
}

void Pebble::setupWebServices(const QString &url)
{
    m_iface->call("SetupWebServices", url);
    if (url.isEmpty()) {
        emit webServicesUpdated();
        emit accountInfoChanged();
    }
}

QString Pebble::accountName() const
{
    return fetchProperty("accountName").toString();
}

QString Pebble::accountEmail() const
{
    return fetchProperty("accountEmail").toString();
}

bool Pebble::syncAppsFromCloud() const
{
    return fetchProperty("syncAppsFromCloud").toBool();
}
void Pebble::setSyncAppsFromCloud(bool enable)
{
    m_iface->call("setSyncAppsFromCloud", enable);
    emit syncAppsFromCloudChanged();
}

bool Pebble::voiceDictationEnabled() const
{
    return fetchProperty("VoiceDictationEnabled").toBool();
}
void Pebble::setVoiceDictationEnabled(bool enabled)
{
    m_iface->call("SetVoiceDictationEnabled", enabled);
    emit voiceDictationEnabledChanged();
}
QStringList Pebble::voiceDictationLanguages()
{
    QDBusReply<QStringList> reply = m_iface->call("VoiceDictationLanguages");
    return reply.value();
}
QString Pebble::voiceDictationLanguage()
{
    return fetchProperty("VoiceDictationLanguage").toString();
}
void Pebble::setVoiceDictationLanguage(QString language)
{
    m_iface->call("SetVoiceDictationLanguage", language);
    emit voiceDictationLanguageChanged();
}

void Pebble::resetTimeline()
{
    m_iface->call("resetTimeline");
}

void Pebble::setTimelineWindow()
{
    m_iface->call("setTimelineWindow", -m_timelienWindowStart, -m_timelienWindowFade, m_timelienWindowEnd);
}

void Pebble::configurationClosed(const QString &uuid, const QString &url)
{
    m_iface->call("ConfigurationClosed", uuid, url);
}

void Pebble::launchApp(const QString &uuid)
{
    m_iface->call("LaunchApp", uuid);
}

void Pebble::requestConfigurationURL(const QString &uuid)
{
    m_iface->call("ConfigurationURL", uuid);
}

void Pebble::removeApp(const QString &uuid)
{
    qDebug() << "should remove app" << uuid;
    m_iface->call("RemoveApp", uuid);
}

void Pebble::installApp(const QString &storeId)
{
    qDebug() << "should install app" << storeId;
    m_iface->call("InstallApp", storeId);
}

void Pebble::sideloadApp(const QString &packageFile)
{
    m_iface->call("SideloadApp", packageFile);
}

void Pebble::sideloadFirmware(const QString packageFile)
{
    m_iface->call("SideloadFirmware", packageFile);
}

QVariant Pebble::fetchProperty(const QString &propertyName) const
{
    QDBusMessage m = m_iface->call(propertyName);
    if (m.type() != QDBusMessage::ErrorMessage && m.arguments().count() == 1)
    {
        qDebug() << "property" << propertyName << m.arguments().first();
        return m.arguments().first();
    }
    qDebug() << "error getting property:" << propertyName << m.errorMessage();
    return QVariant();
}

void Pebble::dataChanged()
{
    qDebug() << "data changed";
    m_name = fetchProperty("Name").toString();
    m_address = fetchProperty("Address").toString();
    m_serialNumber = fetchProperty("SerialNumber").toString();
    m_serialNumber = fetchProperty("SerialNumber").toString();
    QString hardwarePlatform = fetchProperty("HardwarePlatform").toString();
    if (hardwarePlatform != m_hardwarePlatform)
    {
        m_hardwarePlatform = hardwarePlatform;
        emit hardwarePlatformChanged();
    }
    m_softwareVersion = fetchProperty("SoftwareVersion").toString();
    m_model = fetchProperty("Model").toInt();
    m_recovery = fetchProperty("Recovery").toBool();
    qDebug() << "model is" << m_model;
    emit modelChanged();

    bool connected = fetchProperty("IsConnected").toBool();
    if (connected != m_connected)
    {
        m_connected = connected;
        emit connectedChanged();
    }
    m_timelienWindowStart = -fetchProperty("timelineWindowStart").toInt();
    m_timelienWindowFade = -fetchProperty("timelineWindowFade").toInt();
    m_timelienWindowEnd = fetchProperty("timelineWindowEnd").toInt();
}

void Pebble::pebbleConnected()
{

    dataChanged();
    m_connected = true;
    emit connectedChanged();

    refreshApps();
    refreshNotifications();
    refreshScreenshots();
}

void Pebble::pebbleDisconnected()
{
    m_connected = false;
    emit connectedChanged();
}

void Pebble::notificationFilterChanged(const QString &sourceId, const QString &name, const QString &icon, const int enabled)
{
    m_notifications->insert(sourceId, name, icon, enabled);
}

void Pebble::refreshNotifications()
{
    QDBusMessage m = m_iface->call("NotificationsFilter");
    if (m.type() == QDBusMessage::ErrorMessage || m.arguments().count() == 0)
    {
        qWarning() << "Could not fetch notifications filter" << m.errorMessage();
        return;
    }

    const QDBusArgument &arg = m.arguments().first().value<QDBusArgument>();

    QVariantMap mapEntryVariant;
    arg >> mapEntryVariant;

    foreach (const QString &sourceId, mapEntryVariant.keys())
    {
        const QDBusArgument &arg2 = qvariant_cast<QDBusArgument>(mapEntryVariant.value(sourceId));
        QVariantMap notifEntry;
        arg2 >> notifEntry;
        m_notifications->insert(sourceId, notifEntry.value("name").toString(), notifEntry.value("icon").toString(), notifEntry.value("enabled").toInt());
    }
}

void Pebble::setNotificationFilter(const QString &sourceId, int enabled)
{
    m_iface->call("SetNotificationFilter", sourceId, enabled);
}

void Pebble::forgetNotificationFilter(const QString &sourceId)
{
    m_iface->call("ForgetNotificationFilter", sourceId);
}

void Pebble::moveApp(const QString &uuid, int toIndex)
{
    // This is a bit tricky:
    AppItem *item = m_installedApps->findByUuid(uuid);
    if (!item)
    {
        qWarning() << "item not found";
        return;
    }
    int realToIndex = 0;
    for (int i = 0; i < m_installedApps->rowCount(); i++)
    {
        if (item->isWatchFace() && m_installedApps->get(i)->isWatchFace())
        {
            realToIndex++;
        }
        else if (!item->isWatchFace() && !m_installedApps->get(i)->isWatchFace())
        {
            realToIndex++;
        }
        if (realToIndex == toIndex)
        {
            realToIndex = i + 1;
            break;
        }
    }
    m_iface->call("MoveApp", m_installedApps->indexOf(item), realToIndex);
}

void Pebble::refreshApps()
{

    QDBusMessage m = m_iface->call("InstalledApps");
    if (m.type() == QDBusMessage::ErrorMessage || m.arguments().count() == 0)
    {
        qWarning() << "Could not fetch installed apps" << m.errorMessage();
        return;
    }

    m_installedApps->clear();
    m_installedWatchfaces->clear();

    const QDBusArgument &arg = m.arguments().first().value<QDBusArgument>();

    QVariantList appList;

    arg.beginArray();
    while (!arg.atEnd())
    {
        QVariant mapEntryVariant;
        arg >> mapEntryVariant;

        QDBusArgument mapEntry = mapEntryVariant.value<QDBusArgument>();
        QVariantMap appMap;
        mapEntry >> appMap;
        appList.append(appMap);
    }
    arg.endArray();

    foreach (const QVariant &v, appList)
    {
        AppItem *app = new AppItem(this);
        app->setStoreId(v.toMap().value("storeId").toString());
        app->setUuid(v.toMap().value("uuid").toString());
        app->setName(v.toMap().value("name").toString());
        app->setIcon(v.toMap().value("icon").toString());
        app->setVendor(v.toMap().value("vendor").toString());
        app->setVersion(v.toMap().value("version").toString());
        app->setIsWatchFace(v.toMap().value("watchface").toBool());
        app->setHasSettings(v.toMap().value("hasSettings").toBool());
        app->setIsSystemApp(v.toMap().value("systemApp").toBool());

        if (app->isWatchFace())
        {
            m_installedWatchfaces->insert(app);
        }
        else
        {
            m_installedApps->insert(app);
        }
    }
}

void Pebble::appsSorted()
{
    QStringList newList;
    for (int i = 0; i < m_installedApps->rowCount(); i++)
    {
        newList << m_installedApps->get(i)->uuid();
    }
    for (int i = 0; i < m_installedWatchfaces->rowCount(); i++)
    {
        newList << m_installedWatchfaces->get(i)->uuid();
    }
    m_iface->call("SetAppOrder", newList);
}

void Pebble::refreshScreenshots()
{
    m_screenshotModel->clear();
    QStringList screenshots = fetchProperty("Screenshots").toStringList();
    foreach (const QString &filename, screenshots)
    {
        m_screenshotModel->insert(filename);
    }
}

void Pebble::screenshotAdded(const QString &filename)
{
    qDebug() << "screenshot added" << filename;
    m_screenshotModel->insert(filename);
}

void Pebble::screenshotRemoved(const QString &filename)
{
    m_screenshotModel->remove(filename);
}

void Pebble::refreshFirmwareUpdateInfo()
{
    bool firmwareUpgradeAvailable = fetchProperty("FirmwareUpgradeAvailable").toBool();
    if (firmwareUpgradeAvailable && !m_firmwareUpgradeAvailable)
    {
        m_firmwareUpgradeAvailable = true;
        m_firmwareReleaseNotes = fetchProperty("FirmwareReleaseNotes").toString();
        m_candidateVersion = fetchProperty("CandidateFirmwareVersion").toString();
        qDebug() << "firmare upgrade" << m_firmwareUpgradeAvailable << m_firmwareReleaseNotes << m_candidateVersion;
        emit firmwareUpgradeAvailableChanged();
    }
    else if (!firmwareUpgradeAvailable && m_firmwareUpgradeAvailable)
    {
        m_firmwareUpgradeAvailable = false;
        m_firmwareReleaseNotes.clear();
        ;
        m_candidateVersion.clear();
        emit firmwareUpgradeAvailableChanged();
    }
    bool upgradingFirmware = fetchProperty("UpgradingFirmware").toBool();
    if (m_upgradingFirmware != upgradingFirmware)
    {
        m_upgradingFirmware = upgradingFirmware;
        emit upgradingFirmwareChanged();
    }
}

void Pebble::requestScreenshot()
{
    m_iface->call("RequestScreenshot");
}

void Pebble::removeScreenshot(const QString &filename)
{
    qDebug() << "removing screenshot" << filename;
    m_iface->call("RemoveScreenshot", filename);
}

void Pebble::performFirmwareUpgrade()
{
    m_iface->call("PerformFirmwareUpgrade");
}

void Pebble::dumpLogs(const QString &filename)
{
    m_iface->call("DumpLogs", filename);
}

int Pebble::steps(const QDateTime &startTime, const QDateTime &endTime)
{
    QDBusReply<int> reply = m_iface->call("Steps", startTime, endTime);
    return reply;
}

int Pebble::averageSteps(const QDateTime &startTime, const QDateTime &endTime)
{
    QDBusReply<int> reply = m_iface->call("AverageSteps", startTime, endTime);
    return reply;
}

QVariantList Pebble::sleepDataForDay(const QDateTime &startTime, const QDateTime &endTime)
{
    QDBusReply<QVariantList> reply = m_iface->call("SleepDataForDay", startTime, endTime);
    QVariantList ret;
    for (int i = 0; i < reply.value().length(); i++) {
        QVariantMap data = qdbus_cast<QVariantMap>(reply.value().at(i).value<QDBusArgument>());
        ret.append(data);
    }
    return ret;
}

int Pebble::sleepAverage(const QDate &startDate, const QDate &endDate, quint8 type)
{
    QDBusReply<int> reply = m_iface->call("SleepAverage", startDate, endDate, type);
    return reply;
}

QVariantMap Pebble::averageSleepTimes(const QDate &day)
{
    QDBusReply<QVariantMap> reply = m_iface->call("AverageSleepTimes", day);
    return reply;
}

int Pebble::batteryPercentage()
{
    QDBusReply<int> reply = m_iface->call("BatteryPercentage");
    return reply;
}