#include "webservices.h"

#include "pebble.h"

QNetworkRequest authedRequest(QString url, QString token)
{
    QUrl newUrl = QUrl(url);
    newUrl.setQuery(QString("access_token=%1").arg(token));
    QNetworkRequest req = QNetworkRequest(newUrl);
    return req;
    // will handle all other access token stuff later
}

QJsonObject parseJson(QByteArray data)
{
    QJsonDocument doc = QJsonDocument::fromJson(data);
    QJsonObject obj = doc.object();
    return obj; // error handling comes later
}

WebServices::WebServices(Pebble *pebble):
  m_pebble(pebble)
{
    m_webservicesPath = pebble->storagePath() + "webservices.conf";
    m_ini = new QSettings(m_webservicesPath, QSettings::IniFormat);
    m_nam = new QNetworkAccessManager(this);
}

void WebServices::setupWebServices(QString stage2)
{
    if (stage2.isEmpty()) {
        m_ini->clear();
        return;
    }

    QString url = stage2;
    url.remove("pebble://custom-boot-config-url/");
    int index = url.indexOf("stage2/");
    url.insert(index + 7, "android/v3/1232"); // this is extremely ugly but it works

    QNetworkRequest request;
    request.setUrl(QUrl(url));
    QNetworkReply *reply = m_nam->get(request);
    qWarning() << "sent GET request";
    connect(reply, &QNetworkReply::finished, [this, reply, url]() {
        qWarning() << "finished get request";
        QByteArray data = reply->read(reply->bytesAvailable());
        // hash the json first to see if we got update
        QByteArray hash = QCryptographicHash::hash(data, QCryptographicHash::Md5);

        if (m_ini->value("lastJson").isNull() || m_ini->value("lastJson").toString() != hash.toHex())
            m_ini->setValue("lastJson", hash.toHex());
        else
            return; // Don't parse the json since it's same

        QJsonObject config = parseJson(data)["config"].toObject();

        // Now we parse the JSON.
        QString accessToken = url.split("access_token=")[1];
        setIniValue("oauthToken", accessToken);

        m_ini->beginGroup("Links");
        QJsonObject links = config["links"].toObject();
        setIniValue("authenticationEndpoint", links["authentication/me"].toString());
        setIniValue("languagePackEndpoint", links["i18n/language_packs"].toString());
        m_ini->endGroup();

        // We now need to fetch account info with link we saved
        QNetworkReply *authRpl = m_nam->get(authedRequest(authenticationEndpoint(), oauthToken()));
        connect(authRpl, &QNetworkReply::finished, [this, authRpl]() {
            qWarning() << "got account info data";
            QByteArray data = authRpl->read(authRpl->bytesAvailable());
            QJsonObject object = parseJson(data);
            m_ini->beginGroup("Account");
            setIniValue("name", object["name"].toString());
            setIniValue("email", object["email"].toString());
            setIniValue("id", object["id"].toString());
            m_ini->endGroup();
            emit accountInfoChanged();
            authRpl->deleteLater();
        });

        m_ini->beginGroup("Timeline");
        QJsonObject timelineConfig = config["timeline"].toObject();
        setIniValue("initialSyncEndpoint", timelineConfig["sync_endpoint"].toString());
        setIniValue("syncInterval", timelineConfig["sync_policy_minutes"].toInt());
        setIniValue("subscriptionsListEndpoint", timelineConfig["subscriptions_list"].toString());
        QString subscriptionEndpoint = timelineConfig["subscribe_to_topic"].toString();
        subscriptionEndpoint.remove("$$topic_id$$");
        setIniValue("subscriptionEndpoint", subscriptionEndpoint);
        QString sandboxUserToken = timelineConfig["sandbox_user_token"].toString();
        sandboxUserToken.remove("$$uuid$$");
        setIniValue("sandboxUserToken", sandboxUserToken);
        m_ini->endGroup();

        m_ini->beginGroup("Locker");
        QJsonObject lockerConfig = config["locker"].toObject();
        setIniValue("addEndpoint", lockerConfig["add_endpoint"].toString().remove("$$app_uuid$$"));
        setIniValue("removeEndpoint", lockerConfig["remove_endpoint"].toString().remove("$$app_uuid$$"));
        setIniValue("getEndpoint", lockerConfig["get_endpoint"].toString());
        m_ini->endGroup();

        m_ini->beginGroup("Voice");
        QJsonObject voiceConfig = config["voice"].toObject();
        QJsonArray languages = voiceConfig["languages"].toArray();
        setIniValue("allowed", !languages.isEmpty());
        if (!languages.isEmpty()) {
            m_ini->beginWriteArray("languages");
            for (int i = 0; i < languages.count(); i++) {
                QJsonObject langObj = languages.at(i).toObject();
                m_ini->setArrayIndex(i);
                m_ini->setValue("locale", langObj["four_char_locale"].toString());
                m_ini->setValue("endpoint", "https://" + langObj["endpoint"].toString() + "/NmspServlet/");
            }
            m_ini->endArray();
        }
        setVoiceDictationLanguage("en_US"); // Set English US as default for now
        m_ini->endGroup();

        setIniValue("loggedIn", true);

        emit webServicesUpdated();
        reply->deleteLater();
    });
}

// General
QString WebServices::oauthToken()
{
    return iniValue("oauthToken").toString();
}
bool WebServices::loggedIn()
{
    return iniValue("loggedIn").toBool();
}
bool WebServices::syncFromCloud()
{
    return iniValue("syncFromCloud").toBool();
}
void WebServices::setSyncFromCloud(bool enabled)
{
    setIniValue("syncFromCloud", enabled);
}

// Timeline
QString WebServices::timelineSyncEndpoint()
{
    if (iniValue("syncEndpoint", "Timeline").toString().isEmpty())
        return iniValue("initialSyncEndpoint", "Timeline").toString();
    else
        return iniValue("syncEndpoint", "Timeline").toString();
}
void WebServices::changeSyncUrl(QString url)
{
    setIniValue("syncEndpoint", url, "Timeline");
}
int WebServices::timelineSyncInterval()
{
    return iniValue("syncInterval", "Timeline").toInt();
}
QString WebServices::timelineSubscriptionsListEndpoint()
{
    return iniValue("subscriptionsListEndpoint", "Timeline").toString();
}
QString WebServices::timelineSubscriptionEndpoint()
{
    return iniValue("subscriptionEndpoint", "Timeline").toString();
}
QString WebServices::sandboxUserToken()
{
    return iniValue("sandboxUserToken", "Timeline").toString();
}

// Account info
QString WebServices::accountName()
{
    return iniValue("name", "Account").toString();
}
QString WebServices::accountEmail()
{
    return iniValue("email", "Account").toString();
}
QString WebServices::accountId()
{
    return iniValue("id", "Account").toString();
}

// Links
QString WebServices::authenticationEndpoint()
{
    return iniValue("authenticationEndpoint", "Links").toString();
}
QString WebServices::languagePackEndpoint()
{
    return iniValue("languagePackEndpoint", "Links").toString();
}

// Locker
QString WebServices::lockerAddEndpoint()
{
    return iniValue("addEndpoint", "Locker").toString();
}
QString WebServices::lockerRemoveEndpoint()
{
    return iniValue("removeEndpoint", "Locker").toString();
}
QString WebServices::lockerGetEndpoint()
{
    return iniValue("getEndpoint", "Locker").toString();
}

// Voice
bool WebServices::voiceAllowed()
{
    return iniValue("allowed", "Voice").toBool();
}
bool WebServices::voiceDictationEnabled()
{
    return iniValue("enabled", "Voice").toBool();
}
void WebServices::setVoiceDictationEnabled(bool enabled)
{
    setIniValue("enabled", enabled, "Voice");
}
QStringList WebServices::voiceDictationLanguages()
{
    m_ini->beginGroup("Voice");
    int size = m_ini->beginReadArray("languages");
    QStringList list;
    for (int i = 0; i < size; i++) {
        m_ini->setArrayIndex(i);
        list.append(m_ini->value("locale").toString());
    }
    m_ini->endArray();
    m_ini->endGroup();
    return list;
}
QString WebServices::voiceDictationLanguage()
{
    return iniValue("selectedLanguage", "Voice").toString();
}
void WebServices::setVoiceDictationLanguage(QString language)
{
    setIniValue("selectedLanguage", language, "Voice");
}
QString WebServices::voiceDictationEndpoint()
{
    m_ini->beginGroup("Voice");
    QString language = m_ini->value("selectedLanguage").toString();
    int size = m_ini->beginReadArray("languages");
    QString endpoint;
    for (int i = 0; i < size; i++) {
        m_ini->setArrayIndex(i);
        if (m_ini->value("locale").toString() == language) endpoint = m_ini->value("endpoint").toString();
    }
    m_ini->endArray();
    m_ini->endGroup();
    return endpoint;
}