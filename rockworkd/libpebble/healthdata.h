#ifndef HEALTHDATA_H
#define HEALTHDATA_H

#include <QObject>
#include <QSqlDatabase>

#include "pebble.h"

enum SleepType : quint8 {
    SleepTypeNormal = 1,
    SleepTypeDeep = 2
};

struct StepsRecord {
    quint16 version;
    quint32 timestamp;
    quint32 steps;
    quint16 orientation;
    quint16 intensity;
    quint16 light_intensity;
    quint32 heart_rate;
};

struct OverlayRecord {
    quint16 version;
    SleepType type;
    quint32 offsetUTC;
    quint32 startTime;
    quint32 duration;
};

class HealthData: public QObject
{
    Q_OBJECT
public:

    HealthData(Pebble *pebble);

    int steps(const QDateTime &startTime, const QDateTime &endTime) const;
    int averageSteps(const QDateTime &startTime, const QDateTime &endTime) const;

    QVariantList sleepDataForDay(const QDateTime &startTime, const QDateTime &endTime) const;
    int sleepAverage(const QDate &startDate, const QDate &endDate, SleepType type) const;
    QVariantMap averageSleepTimes(const QDate &day) const;

public slots:
    void addHealthData(const StepsRecord &record);
    void addSleepData(const OverlayRecord &record);

private:
    void initdb();

    Pebble *m_pebble;
    QSqlDatabase m_db;
};

#endif // HEALTHDATA_H
