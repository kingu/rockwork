#include <QObject>
#include <QSettings>

class Pebble;
class DataLoggingEndpoint;

struct BatteryRecord {
    quint8 percentage;
    quint16 millivolts;
    quint8 messageTs;
};

class BatteryLevel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(quint32 batteryPercentage READ batteryPercentage NOTIFY batteryPercentageChanged)
    Q_PROPERTY(quint32 batteryMillivolts READ batteryMillivolts NOTIFY batteryMillivoltsChanged)
public:
    BatteryLevel(Pebble *pebble, DataLoggingEndpoint *dataLoggingEndpoint);

    quint32 batteryPercentage();
    void setBatteryPercentage(quint32 percentage);

    quint32 batteryMillivolts();
    void setBatteryMillivolts(quint32 millivolts);

public slots:
    void handleBatteryStatistics(BatteryRecord &record);

signals:
    void batteryPercentageChanged();
    void batteryMillivoltsChanged();

private:
    Pebble *m_pebble;
    DataLoggingEndpoint *m_dataLogging;
    QSettings *m_ini;

    quint32 m_batteryPercentage = 0;
    quint32 m_batteryMillivolts = 0;
};