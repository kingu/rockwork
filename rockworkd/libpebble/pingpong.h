#include <QObject>

class Pebble;
class WatchConnection;

class PingPong : public QObject
{
    Q_OBJECT

public:
    explicit PingPong(Pebble *pebble, WatchConnection *connection);

    enum PingPongCommand {
        PING_PING = 0,
        PING_PONG = 1
    };

private slots:
    void handlePingPong(const QByteArray &data);

private:
    Pebble *m_pebble;
    WatchConnection *m_connection;
};
