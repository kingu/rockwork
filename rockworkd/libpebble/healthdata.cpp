#include "healthdata.h"
#include "watchdatareader.h"
#include "dataloggingendpoint.h"

#include <QSettings>
#include <QDir>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlResult>

HealthData::HealthData(Pebble *pebble):
    m_pebble(pebble),
    QObject(pebble)
{
    QString dbPath = pebble->storagePath();
    m_db = QSqlDatabase::addDatabase(QStringLiteral("QSQLITE"));
    QDir dir;
    dir.mkpath(dbPath);
    m_db.setDatabaseName(dbPath + "/healthdata.sqlite");
    initdb();
}

int HealthData::steps(const QDateTime &startTime, const QDateTime &endTime) const
{
    QString queryString = QString("SELECT SUM(steps) from healthdata WHERE timestamp >= %1 AND timestamp <= %2").arg(startTime.toMSecsSinceEpoch() / 1000).arg(endTime.toMSecsSinceEpoch() / 1000);
    QSqlQuery query;
    query.exec(queryString);
    qDebug() << "Query:" << queryString;
    if (query.next()) {
        return query.value(0).toInt();
    } else {
        qWarning() << "Error executing query:" << query.lastError().text() << "Query was:" << queryString;
    }
    return 0;
}

int HealthData::averageSteps(const QDateTime &startTime, const QDateTime &endTime) const
{
    int count = 0;
    int steps = 0;
    for (int i = 0; i < 30; i++) {
        QDateTime start = startTime.addDays(-i);
        QDateTime end = endTime.addDays(-i);
        QString queryString = QString("SELECT COUNT(*) from healthdata WHERE (timestamp >= %1 AND timestamp <= %2)").arg(start.toMSecsSinceEpoch() / 1000).arg(end.toMSecsSinceEpoch() / 1000);
        QSqlQuery query;
        query.exec(queryString);
        if (!query.next()) {
            continue;
        }
        if (query.value(0).toInt() == 0) {
            continue;
        }

        queryString = QString("SELECT SUM(steps) from healthdata WHERE (timestamp >= %1 AND timestamp <= %2)").arg(start.toMSecsSinceEpoch() / 1000).arg(end.toMSecsSinceEpoch() / 1000);
        query.exec(queryString);
        if (query.next()) {
            steps += query.value(0).toInt();
            count++;
        } else {
            qWarning() << "Error executing query:" << query.lastError().text() << "Query was:" << queryString;
        }
    }
    return count > 0 ? steps / count : 0;
}

QVariantList HealthData::sleepDataForDay(const QDateTime &startTime, const QDateTime &endTime) const
{
    QString queryString = QString("SELECT * from sleepdata WHERE starttime >= %1 AND starttime <= %2").arg(startTime.toSecsSinceEpoch()).arg(endTime.toSecsSinceEpoch());
    QSqlQuery query;
    bool result = query.exec(queryString);
    if (!result) {
        qWarning() << "Error executing query:" << query.lastError().text() << "Query was:" << queryString;
    }
    QVariantList ret;
    while (query.next()) {
        QVariantMap entry;
        entry.insert("starttime", query.value(0).toLongLong());
        entry.insert("duration", query.value(1).toInt());
        entry.insert("type", query.value(2).toInt());
        ret.append(entry);
    }
    return ret;
}

int HealthData::sleepAverage(const QDate &startDate, const QDate &endDate, SleepType type) const
{
    QDate currentDay = startDate;
    int count = 0;
    int total = 0;
    while (currentDay <= endDate) {
        QDateTime startTime(currentDay);
        QDateTime endTime(endDate);
        QVariantList dayData = sleepDataForDay(startTime, endTime);
        bool haveData = false;
        for (int i = 0; i < dayData.count(); i++) {
            if ((SleepType)dayData.at(i).toMap().value("type").toInt() == type) {
                haveData = true;
                total += dayData.at(i).toMap().value("duration").toInt();
            }
        }
        if (haveData) {
            count++;
        }
        currentDay = currentDay.addDays(1);
    }
    if (count == 0) {
        return 0;
    }
    return total / count;
}

QVariantMap HealthData::averageSleepTimes(const QDate &day) const
{
    bool isWeekend = day.dayOfWeek() >= 6;
    int count = 0;
    quint64 averageFallAsleepTime = 0;
    quint64 averageWakeupTime = 0;
    quint64 averageSleepTime = 0;
    quint64 averageDeepSleep = 0;

    quint64 twentyFour = 24 * 60 * 60 * 1000;
    for (int i = 0; i < 30; i++) {
        if ((isWeekend && day.addDays(-i).dayOfWeek() < 6) || (!isWeekend && day.addDays(-i).dayOfWeek() >= 6)) {
            continue;
        }
        QVariantList dayData = sleepDataForDay(QDateTime(day.addDays(-i)), QDateTime(day.addDays(-i+1)));
        if (dayData.isEmpty()) {
            continue;
        }
        QDateTime fallAsleepTime;
        QDateTime wakeupTime;
        for (int j = 0; j < dayData.count(); j++) {
            QVariantMap entry = dayData.at(j).toMap();
            if ((SleepType)entry.value("type").toInt() == SleepTypeNormal) {
                averageSleepTime += entry.value("duration").toInt();
                QDateTime entryFallAsleepTime = QDateTime::fromMSecsSinceEpoch(entry.value("starttime").toLongLong() * 1000);
                if (fallAsleepTime.isNull() || entryFallAsleepTime < fallAsleepTime) {
                    fallAsleepTime = entryFallAsleepTime;
                }
                QDateTime entryWakeupTime = QDateTime::fromMSecsSinceEpoch((entry.value("starttime").toLongLong() + entry.value("duration").toInt()) * 1000);
                if (wakeupTime.isNull() || entryWakeupTime > wakeupTime) {
                    wakeupTime = entryWakeupTime;
                }
            } else {
                averageDeepSleep += entry.value("duration").toInt();
            }
        }
        count++;
        averageFallAsleepTime += fallAsleepTime.time().msecsSinceStartOfDay();
        if (fallAsleepTime.time() < QTime(12,0,0)) {
            averageFallAsleepTime += twentyFour;
        }
        averageWakeupTime += wakeupTime.time().msecsSinceStartOfDay();
        if (wakeupTime.time() < QTime(18, 0, 0)) {
            averageWakeupTime += twentyFour;
        }

    }
    if (count == 0) {
        qWarning() << "No sleep records available for requested date:" << day;
        return QVariantMap();
    }
    QDateTime sleepDateTime;
    sleepDateTime.setDate(day);
    averageFallAsleepTime = averageFallAsleepTime / count;
    if (averageFallAsleepTime > twentyFour) {
        averageFallAsleepTime -= twentyFour;
        sleepDateTime = sleepDateTime.addDays(-1);
    }
    sleepDateTime.setTime(QTime::fromMSecsSinceStartOfDay(averageFallAsleepTime));

    QDateTime wakeupDateTime;
    wakeupDateTime.setDate(day);
    averageWakeupTime = averageWakeupTime / count;
    if (averageWakeupTime > twentyFour) {
        averageWakeupTime -= twentyFour;
        wakeupDateTime = wakeupDateTime.addDays(-1);
    }
    wakeupDateTime.setTime(QTime::fromMSecsSinceStartOfDay(averageWakeupTime));

    averageSleepTime /= count;
    averageDeepSleep /= count;

    QVariantMap ret;
    ret.insert("fallasleep", sleepDateTime.toMSecsSinceEpoch() / 1000);
    ret.insert("wakeup", wakeupDateTime.toMSecsSinceEpoch() / 1000);
    ret.insert("sleepTime", averageSleepTime);
    ret.insert("deepSleep", averageDeepSleep);
    return ret;
}

#include <QTimeZone>

void HealthData::addHealthData(const StepsRecord &record)
{
    quint8 steps = record.steps;
    if (steps == 0) return; // Throw away the data, useless anyway
    quint8 orientation = record.orientation;
    quint16 intensity = record.intensity;
    quint32 light_intensity = record.light_intensity;
    quint32 timestamp = record.timestamp;

    QSqlQuery query;
    QString queryString = QString("INSERT INTO healthdata (timestamp, steps, orientation, intensity, light_intensity) VALUES (%1, %2, %3, %4, %5)")
            .arg(timestamp)
            .arg(steps)
            .arg(orientation)
            .arg(intensity)
            .arg(light_intensity);
    bool result = query.exec(queryString);

    if (!result) {
        qWarning() << "Error inserting health data:" << query.lastError().text();
        qDebug() << "Query was:" << queryString;
    }
}

void HealthData::addSleepData(const OverlayRecord &record)
{
    SleepType type = record.type;
    quint32 startTime = record.startTime;
    quint32 duration = record.duration;

    QSqlQuery query;
    bool result = query.exec(QString("INSERT INTO sleepdata (starttime, duration, type) VALUES (%1, %2, %3)")
                            .arg(startTime)
                            .arg(duration)
                            .arg(type));
    if (!result) {
        qDebug() << "Error inserting sleep data:" << query.lastError().text();
    }
}

void HealthData::initdb()
{
    m_db.open();
    if (!m_db.open()) {
        qWarning() << "Error opening state database:" << m_db.lastError().driverText() << m_db.lastError().databaseText();
        return;
    }

    if (!m_db.tables().contains(QStringLiteral("healthdata"))) {
        QSqlQuery query;
        query.exec(QString("CREATE TABLE healthdata(timestamp INTEGER UNIQUE, steps INTEGER, orientation INTEGER, intensity INTEGER, light_intensity INTEGER);"));
    }

    if (!m_db.tables().contains(QStringLiteral("sleepdata"))) {
        QSqlQuery query;
        int result = query.exec(QString("CREATE TABLE sleepdata( \
                                        starttime INTEGER, \
                                        duration INTEGER, \
                                        type INTEGER \
                                        );"));
        if (!result) {
            qDebug() << "Error creating sleepdata table:" << query.lastError().text();
        }
    }
}

