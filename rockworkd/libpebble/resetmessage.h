#ifndef RESET_MESSAGE_H
#define RESET_MESSAGE_H

enum ResetMessage {
    ResetMessageRestart = 0x00,
    ResetMessageFactoryReset = 0x02,
    ResetMessagePRF = 0x03 // Not sure what this is
};

#endif
