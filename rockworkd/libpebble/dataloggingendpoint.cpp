#include "dataloggingendpoint.h"

#include "pebble.h"
#include "watchconnection.h"
#include "watchdatareader.h"
#include "watchdatawriter.h"
#include "healthdata.h"
#include "batterylevel.h"

DataLoggingEndpoint::DataLoggingEndpoint(Pebble *pebble, WatchConnection *connection):
    QObject(pebble),
    m_pebble(pebble),
    m_connection(connection)
{
    m_connection->registerEndpointHandler(WatchConnection::EndpointDataLogging, this, "handleMessage");

    QObject::connect(m_connection, &WatchConnection::watchConnected, this, [&]() {
        QByteArray reply;
        WatchDataWriter writer(&reply);
        writer.write<quint8>(DataLoggingReportOpenSessions);
        m_connection->writeToPebble(WatchConnection::EndpointDataLogging, reply);
    });
}

QMap<quint8, DataLoggingSession> DataLoggingEndpoint::sessionsList()
{
    return m_loggingSessions;
}
// This seems to not work, no idea why
void DataLoggingEndpoint::getFullSession(quint8 sessionId)
{
    qWarning() << "getting full session";
    QByteArray data;
    WatchDataWriter writer(&data);

    writer.write<quint8>(DataLoggingEmptySession);
    writer.write<quint8>(sessionId);

    m_connection->writeToPebble(WatchConnection::EndpointDataLogging, data);
}

void DataLoggingEndpoint::handleMessage(const QByteArray &data)
{
    qDebug() << "data logged" << data.toHex();
    WatchDataReader reader(data);
    DataLoggingCommand command = (DataLoggingCommand)reader.read<quint8>();
    quint8 sessionId = reader.read<quint8>();
    switch (command) {
    case DataLoggingDespoolOpenSession: {
        DataLoggingSession session;
        session.appUuid = reader.readUuid();
        session.timestamp = reader.readLE<quint32>();
        quint32 logTag = reader.readLE<quint32>();
        session.itemType = (SessionItemType)reader.read<quint8>();
        session.itemSize = reader.readLE<quint16>();

        if (!m_loggingSessions.contains(sessionId)) {
            if (logTag == 78) {
                session.type = DataLoggingSessionAnalytics;
                m_loggingSessions.insert(sessionId, session);
            } else if (logTag == 81) {
                session.type = DataLoggingSessionHealthSteps;
                m_loggingSessions.insert(sessionId, session);
            } else if (logTag == 83) {
                session.type = DataLoggingSessionHealthSleep;
                m_loggingSessions.insert(sessionId, session);
            } else if (logTag == 84) {
                session.type = DataLoggingSessionHealthOverlayData;
                m_loggingSessions.insert(sessionId, session);
            } else if (logTag == 85) {
                session.type = DataLoggingSessionHealthHR;
                m_loggingSessions.insert(sessionId, session);
            } else {
                session.type = DataLoggingSessionNormal;
                m_loggingSessions.insert(sessionId, session);
            }
        }

        qWarning() << "session opened" << logTag << session.itemSize << sessionId << session.type;

        QByteArray reply;
        WatchDataWriter writer(&reply);
        writer.write<quint8>(DataLoggingACK);
        writer.write<quint8>(sessionId);
        m_connection->writeToPebble(WatchConnection::EndpointDataLogging, reply);

        return;
    }
    case DataLoggingDespoolSendData: {
        quint32 itemsLeft = reader.readLE<quint32>();
        quint32 crc = reader.readLE<quint32>();
        quint32 length = data.length() - 10;
        qWarning() << "Despooling data: Session:" << sessionId << "Items left:" << itemsLeft << "CRC:" << crc << "Bytes:" << data.length();

        if (m_loggingSessions.contains(sessionId)) {
            DataLoggingSession loggingSession = m_loggingSessions.value(sessionId);

            if (loggingSession.type == DataLoggingSessionAnalytics) {
                reader.skip(3);
                quint32 messageTs = reader.readLE<quint32>();

                reader.skip(12);
                quint16 mvolts = reader.readLE<quint16>();

                reader.skip(2);
                quint8 percentage = reader.read<quint8>();

                BatteryRecord record;
                record.percentage = percentage;
                record.millivolts = mvolts;
                record.messageTs = messageTs;
                emit batteryStatisticsChanged(record);

            } else if (loggingSession.type == DataLoggingSessionHealthSteps) {
                quint32 timestamp;
                quint8 recordLength, recordNum;
                quint16 recordVersion;

                int packetCount = length / loggingSession.itemSize;

                if ((length % loggingSession.itemSize) != 0) {
                    qWarning() << "malformed data";
                    return;
                }

                for (int i = 0; i < packetCount; i++) {
                    recordVersion = reader.readLE<quint16>();
                    if ((recordVersion != 5) && (recordVersion != 6) && (recordVersion != 7) && (recordVersion != 12) && (recordVersion != 13)) {
                        return;
                    }

                    timestamp = reader.readLE<quint32>();
                    reader.read<quint8>(); // skip this
                    recordLength = reader.read<quint8>();
                    recordNum = reader.read<quint8>();


                    for (int j = 0; j < recordNum; j++) {
                        StepsRecord record;
                        record.timestamp = timestamp;
                        record.version = recordVersion;

                        // We probably should read the 16 bytes and then use them for reading steps;
                        // but for now this does the trick
                        record.steps = reader.read<quint8>();
                        record.orientation = reader.read<quint8>();
                        record.intensity = reader.readLE<quint16>();
                        quint8 is_invalid = reader.read<quint8>(); // this seems to output quite weird values
                        record.light_intensity = reader.read<quint8>();
                        reader.skip(10); // record length is 16, we use 6 bytes.

                        qWarning() << "have steps data" << record.steps << record.orientation << record.intensity << is_invalid << record.light_intensity << record.timestamp;

                        emit stepsDataChanged(record);

                        timestamp += 60;
                    }
                }

            } else if (loggingSession.type == DataLoggingSessionHealthSleep) {
                int recordCount = length / loggingSession.itemSize;

                if ((length % loggingSession.itemSize) != 0)
                    return;
                
                for (int i = 0; i < recordCount; i++) {
                    QByteArray data = reader.readBytes(loggingSession.itemSize);
                    WatchDataReader sleepDataReader(data);

                    quint16 recordVersion = sleepDataReader.readLE<quint16>();
                    int offsetUTC = sleepDataReader.readLE<quint32>();
                    int bedTimeStart = sleepDataReader.readLE<quint32>();
                    int bedTimeEnd = sleepDataReader.readLE<quint32>();
                    int deepSleepSeconds = sleepDataReader.readLE<quint32>();

                    qWarning() << "have sleep record" << recordVersion << offsetUTC << bedTimeStart << bedTimeEnd << deepSleepSeconds;
                }

            } else if (loggingSession.type == DataLoggingSessionHealthOverlayData) {
                if ((length % loggingSession.itemSize) != 0)
                    return;
                
                int recordCount = length / loggingSession.itemSize;

                for (int i = 0; i < recordCount; i++) {
                    OverlayRecord record;
                    record.version = reader.readLE<quint16>();
                    reader.readLE<quint16>(); // skip this
                    record.type = (SleepType)reader.readLE<quint16>();
                    record.offsetUTC = reader.readLE<quint32>();
                    record.startTime = reader.readLE<quint32>();
                    record.duration = reader.readLE<quint32>();

                    reader.skip(8); // item size is 26 bytes, we read 18. skip the rest

                    qWarning() << "have overlay data" << record.version << record.type << record.offsetUTC << record.startTime << record.duration;
                    emit overlayDataChanged(record);
                }
            }

            qWarning() << "Sending ACK to watch";
            QByteArray reply;
            WatchDataWriter writer(&reply);
            writer.write<quint8>(DataLoggingACK);
            writer.write<quint8>(sessionId);
            m_connection->writeToPebble(WatchConnection::EndpointDataLogging, reply);

        }

        return;
    }
    case DataLoggingCloseSession: {
        if (m_loggingSessions.contains(sessionId))
            m_loggingSessions.remove(sessionId);

        qWarning() << "Closing data logging session.";

        QByteArray reply;
        WatchDataWriter writer(&reply);
        writer.write<quint8>(DataLoggingACK);
        writer.write<quint8>(sessionId);
        m_connection->writeToPebble(WatchConnection::EndpointDataLogging, reply);
        return;
    }
    case DataLoggingTimeout: {
        qDebug() << "DataLogging reached timeout: Session:" << sessionId;
        return;
    }
    default:
        qWarning() << "Unhandled DataLogging message" << command;
    }
}
