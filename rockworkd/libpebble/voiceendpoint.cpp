#include "voiceendpoint.h"

#include "pebble.h"
#include "watchdatawriter.h"
#include "watchdatareader.h"
#include "watchconnection.h"

#include <QTimerEvent>
#include <QDebug>

VoiceEndpoint::VoiceEndpoint(Pebble *pebble, WatchConnection *connection):
    QObject(pebble),
    m_pebble(pebble),
    m_watchConnection(connection)
{
    qDebug() << "Attaching endpoint to bus" << WatchConnection::EndpointVoiceControl << WatchConnection::EndpointAudioStream;
    m_watchConnection->registerEndpointHandler(WatchConnection::EndpointVoiceControl, this, "handleMessage");
    m_watchConnection->registerEndpointHandler(WatchConnection::EndpointAudioStream, this, "handleFrame");
}

void VoiceEndpoint::handleMessage(const QByteArray &data)
{
    WatchDataReader reader(data);
    quint8 cmd = reader.read<quint8>();

    if(cmd == CmdSessionSetup) {
        quint32 flags = reader.readLE<quint32>();
        quint8 sessType = reader.read<quint8>();
        quint16 sesId = reader.readLE<quint16>();
        if(sesId > 0 && m_sessId == 0) {
            AttributeList list = readAttributes(reader);
            if(((flags & FlagAppInitiated)!=0) == list.contains(AttAppUuid)) {
                if(sessType == SesDictation) {
                    qDebug() << "Got Dictation session request" << sesId;
                    if(list.count > 0 && list.contains(AttSpeexInfo)) {
                        m_sessId = sesId;
                        m_sesType = (SessionType)sessType;
                        m_appUuid = list.attByType(AttAppUuid).uuid;
                        m_codec = list.attByType(AttSpeexInfo).speexInfo;
                        qDebug() << "Session Setup Request" << ((flags&FlagAppInitiated)?list.attByType(AttAppUuid).uuid.toString():"");
                        emit sessionSetupRequest(m_appUuid);
                        m_sesPhase = PhSetupRequest;
                        m_sesTimer = startTimer(4000);
                    } else {
                        qWarning() << "Invalid attribute set for dictation request" << list.count;
                    }
                } else if(sessType == SesCommand) {
                    qDebug() << "Got Voice Command session request which is rather unexpected";
                } else {
                    qWarning() << "Unknown Session type" << sessType << data.toHex();
                }
            } else {
                qWarning() << "Invalid Flags/Attributes combination" << flags;
            }
        } else {
            qWarning() << "Invalid sessionID for session setup" << sesId << m_sessId << data.toHex();
        }
    } else {
        qWarning() << "Unknown command" << data.toHex();
    }
}

void VoiceEndpoint::sessionSetupResponse(Result result, const QUuid &appUuid)
{
    if(m_sessId>0) {
        qDebug() << "Sending session setup result" << result << "at session" << m_sessId << "of type" << m_sesType << "with app" << appUuid;
        quint32 flags = appUuid.isNull()?0:FlagAppInitiated;
        QByteArray pkt;
        WatchDataWriter writer(&pkt);
        pkt.append((quint8)CmdSessionSetup);
        writer.writeLE<quint32>(flags);
        pkt.append((quint8)m_sesType);
        pkt.append((quint8)result);
        m_watchConnection->writeToPebble(WatchConnection::EndpointVoiceControl,pkt);
        m_sesPhase = PhSetupComplete;
        if(result != ResSuccess) {
            sessionDestroy();
        } else {
            if(m_sesTimer)
                killTimer(m_sesTimer);
            m_sesTimer = startTimer(2000);
        }
    }
}
/**
 * @brief VoiceEndpoint::handleFrame
 * @param data
 * Handles raw unframed packet for audio endpoint
 * It handles both - bitstream and stop packet
 * When bitstream stops it fires a timer to detect timeout and cleanup session
 * Important: response should be made within 2000ms window after stop
 */
void VoiceEndpoint::handleFrame(const QByteArray &data)
{
    WatchDataReader reader(data);
    quint8 cmd = reader.read<quint8>();
    quint16 sid = reader.readLE<quint16>();

    if (cmd == FrmDataTransfer) {
        if (sid == m_sessId) {
            quint8 count = reader.read<quint8>();
            for (int i = 0; i < count; i++) {
                Frame frm;
                frm.length = reader.read<quint8>();
                frm.data = reader.readBytes(frm.length);
                m_framesList.append(frm);
            }
            m_sesPhase = PhAudioStarted;
        } else {
            stopAudioStream(sid);
        }
    } else if (cmd == FrmStopTransfer) {
        if (sid != m_sessId)
            return;
        if (m_sesTimer)
            killTimer(m_sesTimer);
        qDebug() << "Pebble finished sending audio at session" << m_sessId;
        m_sesPhase = PhAudioStopped;
        m_sesTimer = startTimer(4000);
        if (m_framesList.count() > 0)
            emit audioFramesReceived(m_framesList);
    } else {
        qWarning() << "Unknown audio frame type" << data.toHex();
    }
}

void VoiceEndpoint::stopAudioStream()
{
    stopAudioStream(m_sessId);
}
void VoiceEndpoint::stopAudioStream(quint16 sid)
{
    if(sid>0) {
        qDebug() << "Terminating audio stream for session" << sid;
        QByteArray pkt;
        WatchDataWriter writer(&pkt);
        writer.write<quint8>(FrmStopTransfer);
        writer.writeLE<quint16>(sid);
        m_watchConnection->writeToPebble(WatchConnection::EndpointAudioStream, pkt);
    }
}

void VoiceEndpoint::transcriptionResponse(Result result, const QList<Sentence> &data)
{
    if (m_sessId > 0) {
        qDebug() << "Results submitted with status" << result << "data" << data.count() << "for" << m_appUuid.toString();
        m_result = result;
        if (result == ResSuccess) {
            if (data.count()>0) {
                m_sesResult.append(data);
            }
        }
        if (m_sesPhase == PhAudioStopped) {
            if (m_sesTimer)
                killTimer(m_sesTimer);
            m_sesPhase = PhResultReceived;
            m_sesTimer = startTimer(100);
        }
    }
}

/**
 * @brief VoiceEndpoint::sendDictationResults
 * Important: Pebble accepts only 1 sentence currently, hence need to choose the best one here
 */
void VoiceEndpoint::sendDictationResults()
{
    if (m_sessId > 0) {
        if (m_sesTimer) {
            killTimer(m_sesTimer);
            m_sesTimer = 0;
            m_sesPhase = PhResultSent;
        }
        quint32 flags = m_appUuid.isNull() ? 0 : FlagAppInitiated;
        qDebug() << "Sending session recognition result" << m_result << "for session" << m_sessId << "with content" << m_sesResult.sentences.count() << "for app" << m_appUuid << m_appUuid.isNull();
        QByteArray pkt;
        WatchDataWriter writer(&pkt);
        writer.write<quint8>(CmdDictatResult);
        writer.writeLE<quint32>(flags);
        writer.writeLE<quint16>(m_sessId);
        writer.write<quint8>(m_result);
        AttributeList al;
        if (!m_appUuid.isNull())
            al.append(Attribute(m_appUuid));
        m_sesResult.sort(1);
        if (m_result == ResSuccess)
            al.append(Attribute(Transcription(m_sesResult)));
        al.writerWrite(writer);
        m_watchConnection->writeToPebble(WatchConnection::EndpointVoiceControl,pkt);
        qDebug() << "Sent" << pkt.toHex();
    }
}
void VoiceEndpoint::sessionDestroy()
{
    if (m_sesTimer) {
        killTimer(m_sesTimer);
        m_sesTimer = 0;
    }
    m_appUuid = QUuid();
    m_sesPhase = PhSessionClosed;
    m_sesResult.sentences.clear();
    m_result = ResSuccess;
    m_framesList.clear();
    m_sessId = 0;
    qDebug() << "Session closed, state reset to initial";
}

void VoiceEndpoint::timerEvent(QTimerEvent *event)
{
    if(m_sesTimer > 0 && event && event->timerId() == m_sesTimer) {
        killTimer(m_sesTimer);
        m_sesTimer = 0;
        switch (m_sesPhase) {
        case PhSetupRequest:
            sessionSetupResponse(ResTimeout,QUuid());
            break;
        case PhSetupComplete:
            sessionSetupResponse(ResInvalidMessage,QUuid());
            break;
        case PhAudioStarted:
            stopAudioStream(m_sessId);
            break;
        case PhAudioStopped:
        case PhResultReceived:
            sendDictationResults();
            sessionDestroy();
            break;
        default:
            qDebug() << "Unhandled timer event for phase" << m_sesPhase;
        }
    }
}

/**
 * @brief VoiceEndpoint::AttributeList
 * @return
 */
VoiceEndpoint::AttributeList::AttributeList():
    count(0)
{
    //
}
void VoiceEndpoint::AttributeList::append(const Attribute &att)
{
    m_aidx.insert(att.type,(quint8)m_atts.size());
    m_atts.append(att);
}
const VoiceEndpoint::Attribute& VoiceEndpoint::AttributeList::attByNum(quint8 num) const
{
    return m_atts.at(num);
}
bool VoiceEndpoint::AttributeList::contains(AttributeType type) const
{
    return m_aidx.contains(type);
}
static const VoiceEndpoint::Attribute invalidAttr;
const VoiceEndpoint::Attribute& VoiceEndpoint::AttributeList::attByType(AttributeType type) const
{
    if(m_aidx.contains(type))
        return m_atts.at(m_aidx.value(type));
    return invalidAttr;
}
void VoiceEndpoint::AttributeList::writerWrite(WatchDataWriter &writer)
{
    count = m_atts.count();
    writer.write<quint8>(count);
    for(int i=0;i<count;i++) {
        qDebug() << "Writing attribute" << m_atts.at(i).toString();
        m_atts[i].writerWrite(writer);
    }
}

VoiceEndpoint::Attribute::Attribute(const Transcription &tr):
    type(VoiceEndpoint::AttTranscription),
    length(0),
    transcription(tr)
{
}
VoiceEndpoint::Attribute::Attribute(const QUuid &appUuid):
    type(AttAppUuid),
    length(16),
    uuid(appUuid)
{
}
VoiceEndpoint::Attribute::Attribute(const SpeexInfo &codec):
    type(AttSpeexInfo),
    length(29),
    speexInfo(codec)
{
}
VoiceEndpoint::Attribute::Attribute(quint8 type, quint16 length, const QByteArray &data):
    type(type),
    length(length),
    buf(data)
{
}

QByteArray& VoiceEndpoint::Attribute::serialize(QByteArray &in)
{
    if(buf.length()>0)
        in.append(buf);
    else {
        WatchDataWriter writer(&in);
        writerWrite(writer);
    }
    return in;
}
void VoiceEndpoint::Attribute::writerWrite(WatchDataWriter &writer)
{
    writer.write<quint8>(type);
    if(buf.length()>0) {
        writer.writeLE<quint16>(buf.length());
        writer.writeBytes(buf.length(),buf);
    } else if(type == AttTranscription) {
        qDebug() << "Serializing Transcription" << length << transcription.type;
        if(length) {
            writer.writeLE<quint16>(length);
            transcription.writerWrite(writer);
        } else {
            QByteArray _buf;
            _buf = transcription.serialize(_buf);
            length = _buf.length();
            writer.writeLE<quint16>(length);
            writer.writeBytes(length,_buf);
        }
    } else if(type == AttAppUuid) {
        writer.writeLE<quint16>(16);
        writer.writeUuid(uuid);
    } else if(type == AttSpeexInfo) {
        writer.writeLE<quint16>(29);
        writer.writeBytes(20,speexInfo.version);
        writer.writeLE<quint32>(speexInfo.sampleRate);
        writer.writeLE<quint16>(speexInfo.bitRate);
        writer.write<quint8>(speexInfo.bitstreamVer);
        writer.writeLE<quint16>(speexInfo.frameSize);
    } else {
        qWarning() << "Serialisation impossible for zero type" << type;
    }
}
QString VoiceEndpoint::Attribute::toString() const
{
    return QString("Attribute(type=%1, %2)").arg(QString::number(type),(
                (type==AttSpeexInfo)?QString("SpeexInfo()"):
                                     (type==AttAppUuid)?uuid.toString():
                                                        (type==AttTranscription)?QString("Transcription(%1)").arg(transcription.toString()):
                                                                                 QString("UnknownAttribute(QByteArray(%1))").arg(QString(buf))
                ));
}

QByteArray& VoiceEndpoint::Transcription::serialize(QByteArray &buf) const
{
    WatchDataWriter writer(&buf);
    writerWrite(writer);
    return buf;
}
void VoiceEndpoint::Transcription::writerWrite(WatchDataWriter &writer) const
{
    writer.write<quint8>(type);
    sentenceList.writerWrite(writer);
}
QString VoiceEndpoint::Transcription::toString() const
{
    return QString("type=%1, %2").arg((type==TrSentenceList)?"SentenceList":QString::number(type)).arg(sentenceList.toString());
}

QByteArray& VoiceEndpoint::SentenceList::serialize(QByteArray &buf) const
{
    WatchDataWriter writer(&buf);
    writerWrite(writer);
    return buf;
}
void VoiceEndpoint::SentenceList::writerWrite(WatchDataWriter &writer) const
{
    writer.write<quint8>(count);
    for(int i=0;i<count;i++) {
        Sentence st = sentences.at(i);
        writer.writeLE<quint16>(st.count);
        if(st.count>st.words.count()) st.count = st.words.count();
        for(int j=0;j<st.count;j++) {
            Word word = st.words.at(j);
            writer.write<quint8>(word.confidence);
            writer.writeLE<quint16>(word.length);
            writer.writeBytes(word.length,word.data);
        }
    }
}
void VoiceEndpoint::SentenceList::sort(int trim)
{
    if(trim>0 && sentences.count()==1) {
        count = 1;
        return;
    }
    count = (quint8)qMin(255,qMin(trim,sentences.count()));
    QList<quint8> avg;
    for(int i=0;i<count;i++) {
        int m=i;
        int sum=0;
        for(int k=0;k<sentences.at(i).words.count();k++){
            sum+=sentences.at(i).words.at(k).confidence;
        }
        avg.append(sum / sentences.at(m).words.count());
        for(int j=i+1;j<sentences.count();j++) {
            if(avg.count()<=j) {
                sum=0;
                for(int k=0;k<sentences.at(j).words.count();k++){
                    sum+=sentences.at(j).words.at(k).confidence;
                }
                avg.append(sum / sentences.at(j).words.count());
            }
            if(avg.at(m) < avg.at(j))
                m=j;
        }
        if(m!=i) {
            Sentence s=sentences.at(i);
            sentences[i]=sentences[m];
            sentences[m]=s;
            avg[m]=avg[i];
        }
    }
}
QString VoiceEndpoint::SentenceList::toString() const
{
    QString sl;
    foreach(const Sentence &s,sentences) {
        QString wl;
        foreach(const Word &w,s.words) {
            wl += QString("Word(confidence=%1, word=%2), ").arg(QString::number(w.confidence),QString::fromUtf8(w.data));
        }
        sl += QString("Sentence(count=%1, words=[%2]), ").arg(QString::number(s.count),wl);
    }
    return QString("SentenceList(count=%1, sentences=[%2])").arg(QString::number(count),sl);
}

VoiceEndpoint::AttributeList VoiceEndpoint::readAttributes(WatchDataReader &reader)
{
    AttributeList list;
    list.count = reader.read<quint8>();
    for(int i=0;i<list.count;i++) {
        Attribute att;
        att.type = reader.read<quint8>();
        att.length = reader.readLE<quint16>();
        switch(att.type) {
        case AttSpeexInfo:
            att.speexInfo.version = reader.readBytes(20);
            att.speexInfo.sampleRate = reader.readLE<quint32>();
            att.speexInfo.bitRate = reader.readLE<quint16>();
            att.speexInfo.bitstreamVer = reader.read<quint8>();
            att.speexInfo.frameSize = reader.readLE<quint16>();
            break;
        case AttAppUuid:
            att.uuid = reader.readUuid();
            break;
        case AttTranscription:
            att.transcription.type = reader.read<quint8>();
            att.transcription.sentenceList.count = reader.read<quint8>();
            for(int j=0;j<att.transcription.sentenceList.count;j++) {
                Sentence st;
                st.count = reader.readLE<quint16>();
                for(int k=0;k<st.count;k++) {
                    Word word;
                    word.confidence = reader.read<quint8>();
                    word.length = reader.readLE<quint16>();
                    word.data = reader.readBytes(word.length);
                    st.words.append(word);
                }
                att.transcription.sentenceList.sentences.append(st);
            }
            break;
        default:
            att.buf = reader.readBytes(att.length);
            qWarning() << "Unknown attribute type" << att.type << att.buf.toHex();
        }
        list.append(att);
    }
    return list;
}
