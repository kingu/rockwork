#include "batterylevel.h"

#include "pebble.h"
#include "dataloggingendpoint.h"

BatteryLevel::BatteryLevel(Pebble *pebble, DataLoggingEndpoint *dataLoggingEndpoint):
  m_pebble(pebble),
  m_dataLogging(dataLoggingEndpoint)
{
    m_ini = new QSettings(pebble->storagePath() + "/watchinfo.conf", QSettings::IniFormat);
    if (m_batteryPercentage == 0) {
        // Restore last-known percentage until we get new data
        setBatteryPercentage(m_ini->value("LastKnownPercentage").toInt());
        setBatteryMillivolts(m_ini->value("LastKnownMillivolts").toInt());
    }
}

quint32 BatteryLevel::batteryPercentage()
{
    return m_batteryPercentage;
}
void BatteryLevel::setBatteryPercentage(quint32 percentage)
{
    m_batteryPercentage = percentage;
    emit batteryPercentageChanged();
}

quint32 BatteryLevel::batteryMillivolts()
{
    return m_batteryMillivolts;
}
void BatteryLevel::setBatteryMillivolts(quint32 millivolts)
{
    m_batteryMillivolts = millivolts;
    emit batteryMillivoltsChanged();
}

void BatteryLevel::handleBatteryStatistics(BatteryRecord &record)
{
    if (record.messageTs > 0 && record.millivolts < 5000) {
        m_batteryPercentage = record.percentage;
        m_ini->setValue("LastKnownPercentage", record.percentage);
        m_batteryMillivolts = record.millivolts;
        m_ini->setValue("LastKnownMillivolts", record.millivolts);
    }
}