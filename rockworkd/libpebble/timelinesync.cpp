#include "timelinesync.h"
#include "timelinemanager.h"
#include "pebble.h"
#include "webservices.h"

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrlQuery>
#include <QSettings>

/**
 * @brief TimelineSync::TimelineSync
 * @param pebble
 * @param manager
 *
 * TimelineSync class starts a timer to run a websync task, to pull pins from pebble.com. For
 * authorisation it uses OAuth2 token which is locally stored. Valid token should be set by
 * pebble class i.e. via DBus. If token results in Auth.Error - token is cleared and stored.
 * Cleared (empty) token means websync is disabled (stops the timer).
 *
 * Since the class is responsible for timeline API calls, it also executes JSKit timeline calls.
 * And for that it also syncs and handles pabble locker API. Simply because no one else cares
 * about the locker or need it. As a side effect - it will install all the apps registered in the
 * locker but missing in the local configuration - i.e. "keep the apps in sync with the cloud".
 * Obviously all these actions are possible only with valid OAuth token.
 *
 * All the configuration is preserved in QSettings based file timeline/manager.ini
 */
TimelineSync::TimelineSync(Pebble *pebble, WebServices *services, TimelineManager *manager):
  QObject(pebble),
  m_nam(pebble->nam()),
  m_pebble(pebble),
  m_webServices(services),
  m_manager(manager)
{
    m_timelineStoragePath = pebble->storagePath() + "timeline";
    m_syncUrl = m_webServices->timelineSyncEndpoint();
    m_oauthToken = m_webServices->oauthToken();
    m_subscriptionsUrl = m_webServices->timelineSubscriptionsListEndpoint();
    m_sandboxToken = m_webServices->sandboxUserToken();

    m_lockerAddEndpoint = m_webServices->lockerAddEndpoint();
    m_lockerRemoveEndpoint = m_webServices->lockerRemoveEndpoint();
    m_lockerGetEndpoint = m_webServices->lockerGetEndpoint();
    m_syncFromCloud = m_webServices->syncFromCloud();

    m_syncInterval = m_webServices->timelineSyncInterval();

    // Make connection to itself queued to prevent recursive closure and yield to event loop
    connect(this, &TimelineSync::timelineOp, this, &TimelineSync::webOpHandler, Qt::QueuedConnection);
    connect(this,&TimelineSync::wipePinKind,manager,&TimelineManager::wipeTimeline,Qt::QueuedConnection);
    // websync needs more frequent update to keep apps up-to-whatever-they-need
    if (!m_oauthToken.isEmpty())
        m_tmr_websync = startTimer(m_syncInterval * 60 * 60 * 1000);
}

QNetworkRequest TimelineSync::authedRequest(const QString &url, const QString &token) const
{
    QUrl newUrl = QUrl(url);
    QUrlQuery query(newUrl);
    query.addQueryItem("access_token", token);
    newUrl.setQuery(query);
    QNetworkRequest req = QNetworkRequest(newUrl);
    return req;
}

QJsonObject processJsonReply(QNetworkReply *rpl, QString &err)
{
    rpl->deleteLater();
    if(rpl && rpl->error() == QNetworkReply::NoError) {
        QByteArray data = rpl->read(rpl->bytesAvailable());
        QJsonParseError jpe;
        QJsonDocument doc = QJsonDocument::fromJson(data,&jpe);
        if(jpe.error == QJsonParseError::NoError && doc.isObject() && !doc.object().isEmpty()) {
            QJsonObject obj = doc.object();
            if(obj.contains("error") || obj.contains("errorString")) {
                err.append("Response contains error: ").append(obj.value("error").toString()).append(obj.value("errorString").toString());
            } else {
                return obj;
            }
        } else {
            err.append(QString("Cannot parse response: %1 %2").arg(jpe.errorString(),QString(data)));
            qDebug() << "Cannot parse" << data;
        }
    } else {
        err.append("HTTP Error: ").append(rpl->errorString());
    }
    return QJsonObject();
}

void TimelineSync::timerEvent(QTimerEvent *event)
{
    if (event) {
        if (!m_pendingReply.isNull())
            m_pendingReply->abort();
        doWebsync();
    }
}

void TimelineSync::doWebsync()
{
    if(m_oauthToken.isEmpty()) {
        qDebug() << "No valid authentication token, skipping WebSync";
        return;
    }
    if (!m_tmr_websync) {
        qDebug() << "Starting timeline sync timer";
        m_tmr_websync = startTimer(m_syncInterval * 60);
    }
    // Attempt to bring up internal state since qt is not tracking it properly
    if(m_nam->networkAccessible()!=QNetworkAccessManager::Accessible)
        m_nam->setNetworkAccessible(QNetworkAccessManager::Accessible);
    QNetworkReply *rpl = m_nam->get(authedRequest(m_syncUrl, m_oauthToken));
    m_pendingReply = rpl;
    connect(rpl,&QNetworkReply::finished,[this,rpl](){
        QString err;
        QJsonObject obj = processJsonReply(rpl,err);
        if(!err.isEmpty()) {
            qWarning() << "Cannot parse response" << err << " " << m_syncUrl;
            return;
        }
        if(obj.contains("error")) {
            qCritical() << "Unknown error received" << obj.value("error").toString() << obj;
            return;
        }
        if (obj.value("mustResync").toBool()) {
            qWarning() << "timeline server forced URL change";
            emit wipePinKind("web");
            emit syncUrlChanged(obj.value("syncURL").toString());
            return;
        }
        QJsonArray arr = obj.value("updates").toArray();
        if(arr.count()>0) {
            qDebug() << "Got stuff to work on" << obj;
            for(int i=0;i<arr.size();i++) {
                emit timelineOp(arr.at(i).toObject());
            }
        }
        if (obj.contains("nextPageURL")) {
            emit syncUrlChanged(obj.value("nextPageURL").toString());
        } else {
            m_syncUrl = obj.value("syncURL").toString();
            emit syncUrlChanged(m_syncUrl);
        }
    });
}

void TimelineSync::handleWebServicesUpdate()
{
    if (!m_oauthToken.isEmpty()) return;

    // I can probably make this cleaner, but for the time being it suffices.
    m_syncUrl = m_webServices->timelineSyncEndpoint();
    m_oauthToken = m_webServices->oauthToken();
    m_subscriptionsUrl = m_webServices->timelineSubscriptionsListEndpoint();
    m_sandboxToken = m_webServices->sandboxUserToken();
    m_lockerAddEndpoint = m_webServices->lockerAddEndpoint();
    m_lockerRemoveEndpoint = m_webServices->lockerRemoveEndpoint();
    m_lockerGetEndpoint = m_webServices->lockerGetEndpoint();
    m_syncInterval = m_webServices->timelineSyncInterval();

    m_locker.clear();
    fetchLocker();
    doWebsync();
}

void TimelineSync::webOpHandler(const QJsonObject &op)
{
    QString opt = op.value("type").toString();
    if(opt.isEmpty()) {
        qWarning() << "Empty operation type, skipping" << opt;
    } else if(opt == "timeline.pin.create") {
        m_manager->insertTimelinePin(op.value("data").toObject());
    } else if(opt == "timeline.pin.delete") {
        m_manager->removeTimelinePin(op.value("data").toObject().value("guid").toString());
    } else if(opt == "timeline.topic.subscribe") {
        qDebug() <<  "Asked to subscribe but we have nothing to do here:" << op.value("data").toObject().value("topicKey");
    } else if(opt == "timelime.topic.unsubscribe") {
        m_manager->wipeSubscription(op.value("data").toObject().value("topicKey").toString());
    } else {
        qWarning() << "Unknown operation type, skipping" << opt;
    }
}

void TimelineSync::timelineApiQuery(const QByteArray &verb, const QString &url, void *ctx, void (*ack)(void *, const QJsonObject &), void (*nack)(void *, const QString &), const QString &token) const
{
    qDebug() << "API call for" << verb << url << token;
    QNetworkReply *rpl = m_nam->sendCustomRequest(authedRequest(url, m_oauthToken),verb);
    connect(rpl,&QNetworkReply::finished,[this,rpl,ctx,ack,nack](){
        QString err;
        QJsonObject obj=processJsonReply(rpl,err);
        if(obj.isEmpty()) {
            if(nack)
                nack(ctx,err);
            else
                qWarning() << err;
        } else {
            ack(ctx,obj);
        }
    });
}

void TimelineSync::syncLocker(bool force)
{
    struct ctx {
        TimelineSync *me;
        bool force;
    } * context = new ctx({this,force});
    fetchLocker(force,[](void*p){
        ((struct ctx*)p)->me->resyncLocker(((struct ctx*)p)->force);
        delete ((struct ctx*)p);
    },context);
}
void TimelineSync::fetchLocker(bool force, void (*next)(void*), void *ctx) const
{
    if(!m_oauthToken.isEmpty() && (m_locker.isEmpty() || force)) {
        qDebug() << "Fetching locker content" << force;
        // Attempt to bring up internal state since qt is not tracking it properly
        if(m_nam->networkAccessible()!=QNetworkAccessManager::Accessible)
            m_nam->setNetworkAccessible(QNetworkAccessManager::Accessible);
        QNetworkReply *rpl = m_nam->get(authedRequest(m_lockerGetEndpoint, m_oauthToken));
        connect(rpl,&QNetworkReply::finished, [this,rpl,next,ctx](){
            QString err;
            QJsonObject obj = processJsonReply(rpl,err);
            if(!obj.isEmpty()) {
                if(obj.contains("applications")) {
                    QJsonArray arr = obj.value("applications").toArray();
                    for(int i=0;i<arr.size();i++) {
                        QJsonObject el(arr.at(i).toObject());
                        el.remove("hardware_platforms"); // big and useless atm
                        qDebug() << "Adding to locker" << el.value("title").toString();
                        m_locker.insert(QUuid(el.value("uuid").toString()),el);
                    }
                } else {
                    qWarning() << "Response does not contain applications array:" << QJsonDocument(obj).toJson();
                }
            } else
                qWarning() << err;
            if(next)
                next(ctx);
        });
    } else if(next)
        next(ctx);
}
void TimelineSync::resyncLocker(bool force)
{
    QList<QUuid> toRemove;
    if(m_oauthToken.isEmpty()) return;
    // Attempt to bring up internal state since qt is not tracking it properly
    if(m_nam->networkAccessible()!=QNetworkAccessManager::Accessible)
        m_nam->setNetworkAccessible(QNetworkAccessManager::Accessible);
    qDebug() << "Locker resync: first handle apps missing here:" << (m_syncFromCloud ? "install missing" : "clean the locker");
    if(!m_locker.isEmpty() && (force || m_syncFromCloud)) {
        for(QHash<QUuid,QJsonObject>::const_iterator it=m_locker.begin();it!=m_locker.end();it++) {
            if(!m_pebble->appInfo(it.key()).isValid()) {
                if(force) {
                    toRemove.append(it.key());
                } else { // No force means we're left to sync apps from cloud
                    qDebug() << "Syncing" << it.key().toString() << it.value().value("title").toString() << "from locker";
                    m_pebble->installApp(it.value().value("id").toString());
                }
            }
        }
    }

    QSet<QUuid> missing = m_pebble->installedAppIds().toSet().subtract(m_locker.keys().toSet());
    qDebug() << "Locker resync: next push to the locker those" << missing.size() << "missing there";
    foreach(const QUuid &id,missing) {
        if(m_pebble->appInfo(id).isSystemApp()) continue;
        QNetworkReply *rpl = m_nam->put(authedRequest(m_lockerAddEndpoint+id.toString().mid(1,36), m_oauthToken),QByteArray());
        connect(rpl,&QNetworkReply::finished,[this,rpl](){
            QString err;
            QJsonObject obj=processJsonReply(rpl,err);
            if(obj.isEmpty()) {
                qWarning() << err;
            } else {
                if(obj.contains("application")) {
                    QJsonObject el(obj.value("application").toObject());
                    el.remove("hardware_platforms");
                    m_locker.insert(QUuid(el.value("uuid").toString()),el);
                    qDebug() << "Registered" << el.value("uuid").toString() << el.value("title").toString() << "to the locker";
                } else {
                    qWarning() << "Error adding application to locker - empty reply:" << QJsonDocument(obj).toJson();
                }
            }
        });
    }

    foreach(const QUuid &id, toRemove) {
        qDebug() << "Removing" << id << m_locker.value(id).value("title").toString() << "from locker";
        QNetworkReply *rpl = m_nam->deleteResource(authedRequest(m_lockerRemoveEndpoint + id.toString().mid(1,36), m_oauthToken));
        connect(rpl,&QNetworkReply::finished,[this,&id,rpl](){
            rpl->deleteLater();
            if(rpl->error()==QNetworkReply::NoError)
                m_locker.remove(id);
            else
                qWarning() << "Error deleting" << id << rpl->errorString();
        });
    }
}
