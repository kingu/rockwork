#include "lomiri-app-launch.h"

LomiriAppLaunch::LomiriAppLaunch()
{
}

LomiriAppLaunch::~LomiriAppLaunch()
{
}

QString LomiriAppLaunch::name() const
{
    return m_name;
}

QString LomiriAppLaunch::icon() const
{
    return m_icon;
}

bool LomiriAppLaunch::populate(const QString &id)
{
  try {
    auto appid = lomiri::app_launch::AppID::find(id.toUtf8().constData());

    if (appid.empty()) {
      return false;
    }

    std::shared_ptr<lomiri::app_launch::Application> app = lomiri::app_launch::Application::create(
          appid, lomiri::app_launch::Registry::getDefault());

    if (!app) {
      return false;
    }

    auto info = app->info();

    m_icon = QString::fromStdString(info->iconPath().value());
    m_name = QString::fromStdString(info->name().value());

    return true;
  } catch (std::runtime_error &e) {
    return false;
  }
}
