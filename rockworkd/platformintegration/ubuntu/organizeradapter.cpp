#include "organizeradapter.h"
#include "libpebble/platforminterface.h"

#include <QOrganizerItemFetchRequest>
#include <QDebug>
#include <QOrganizerEventOccurrence>
#include <QOrganizerItemDetail>
#include <QJsonArray>
#include <QJsonObject>

QTORGANIZER_USE_NAMESPACE

#define MANAGER           "eds"
#define MANAGER_FALLBACK  "memory"

OrganizerAdapter::OrganizerAdapter(QObject *parent) : QObject(parent)
{
    QString envManager(qgetenv("ALARM_BACKEND"));
    if (envManager.isEmpty())
        envManager = MANAGER;
    if (!QOrganizerManager::availableManagers().contains(envManager)) {
        envManager = MANAGER_FALLBACK;
    }
    m_manager = new QOrganizerManager(envManager);
    m_manager->setParent(this);
    connect(m_manager, &QOrganizerManager::itemsChanged, this, &OrganizerAdapter::refresh);

    startTimer(24 * 60 * 60 * 1000);
}

void OrganizerAdapter::reSync(qint32 end)
{
    m_track.clear();
    m_disabled = false;
    m_windowEnd = end;
    refresh();
}

void OrganizerAdapter::disable()
{
    m_disabled = true;
}

void OrganizerAdapter::timerEvent(QTimerEvent *event)
{
    if (event)
        refresh();
}

void OrganizerAdapter::refresh()
{
    if(m_disabled)
        return;
    QStringList todel = m_track.keys(); 

    QList<QOrganizerCollectionId> selectedCollections;
    foreach (const QOrganizerCollection &collection, m_manager->collections()) {
        bool selected = collection.metaData(QtOrganizer::QOrganizerCollection::MetaDataKey::KeyExtended).toMap().value("collection-selected").toBool();
        if (selected) {
            selectedCollections << collection.id();
        }
    }

    foreach (const QOrganizerItem &item, m_manager->items()) {
        if (selectedCollections.contains(item.collectionId())) {
            QOrganizerEvent organizerEvent(item);
            if (organizerEvent.displayLabel().isEmpty()) {
                continue;
            }
            QJsonObject calPin,pinLayout,actSnooze,actOpen;
            QJsonArray reminders,actions;
            QStringList headings,paragraphs;

            headings.append("Calendar");
            if (organizerEvent.recurrenceRules().count() > 0) {
                QString id = organizerEvent.id().toString() + QString::number(organizerEvent.startDateTime().toUTC().toTime_t());
                calPin.insert("id", id);
                calPin.insert("guid",PlatformInterface::idToGuid(id).toString().mid(1,36));
                pinLayout.insert("displayRecurring",QString("recurring"));
            } else {
                QString id = organizerEvent.id().toString();
                calPin.insert("guid", PlatformInterface::idToGuid(id).toString());
                calPin.insert("id", id);
            }

            if (m_track.contains(calPin.value("guid").toString())) {
                todel.removeAll(calPin.value("guid").toString());
                if (m_track.value(calPin.value("guid").toString()).toUTC() == organizerEvent.startDateTime().toUTC())
                    continue;
            }

            m_track.insert(calPin.value("guid").toString(),organizerEvent.startDateTime());

            pinLayout.insert("type",QString("calendarPin"));
            pinLayout.insert("title",organizerEvent.displayLabel());
            pinLayout.insert("body",organizerEvent.description());

            calPin.insert("createTime",organizerEvent.startDateTime().toString(Qt::ISODate));
            calPin.insert("updateTime",organizerEvent.startDateTime().toString(Qt::ISODate));
            calPin.insert("time",organizerEvent.startDateTime().toString(Qt::ISODate));
            calPin.insert("duration",(organizerEvent.endDateTime().toMSecsSinceEpoch() - organizerEvent.startDateTime().toMSecsSinceEpoch()) / 1000 / 60);
            calPin.insert("dataSource",QString("calendarEvent:%1").arg(PlatformInterface::SysID));

            if (organizerEvent.isAllDay())
                calPin.insert("allDay", true);

            pinLayout.insert("locationName",organizerEvent.location());

            if (!organizerEvent.comments().isEmpty()) {
                headings.append("Comments");
                paragraphs.append(organizerEvent.comments().join(";"));
            }

            QStringList attendees;
            foreach (const QOrganizerItemDetail &attendeeDetail, organizerEvent.details(QOrganizerItemDetail::TypeEventAttendee)) {
                attendees.append(attendeeDetail.value(QOrganizerItemDetail::TypeEventAttendee + 1).toString());
            }

            if (!attendees.isEmpty()) {
                headings.append("Attendees");
                paragraphs.append(attendees.join(", "));
            }

            if(!headings.isEmpty()) {
                pinLayout.insert("headings",QJsonArray::fromStringList(headings));
                pinLayout.insert("paragraphs",QJsonArray::fromStringList(paragraphs));
            }
            calPin.insert("layout",pinLayout);

            quint64 startTimestamp = QDateTime::currentMSecsSinceEpoch();
            startTimestamp -= (1000 * 60 * 60 * 24 * 7);

            foreach (const QOrganizerItem &occurranceItem, m_manager->itemOccurrences(item, QDateTime::fromMSecsSinceEpoch(startTimestamp), QDateTime::currentDateTime().addDays(7))) {
                QOrganizerEventOccurrence organizerOccurrance(occurranceItem);
                calPin.insert("id",organizerOccurrance.id().toString());
                calPin.insert("time",organizerOccurrance.startDateTime().toUTC().toString(Qt::ISODate));
                //event.setEndTime(organizerOccurrance.endDateTime());
            }

            actOpen.insert("type",QString("open"));
            actOpen.insert("title",QString("Open"));
            actions.append(actOpen);
            actSnooze.insert("type",QString("snooze"));
            actSnooze.insert("title",QString("Snooze"));
            actions.append(actSnooze);
            calPin.insert("actions",actions);

            emit newTimelinePin(calPin);
        }
    }

    if(!todel.isEmpty()) {
        foreach(const QString &key,todel) {
            m_track.remove(key);
            emit delTimelinePin(key);
        }
    }
}
