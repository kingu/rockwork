#ifndef ORGANIZERADAPTER_H
#define ORGANIZERADAPTER_H

#include <QObject>

#include <QOrganizerManager>
#include <QOrganizerAbstractRequest>
#include <QOrganizerEvent>

QTORGANIZER_USE_NAMESPACE

class OrganizerAdapter : public QObject
{
    Q_OBJECT
public:
    explicit OrganizerAdapter(QObject *parent = 0);

public slots:
    void refresh();
    void reSync(qint32 end);
    void disable();

signals:
    void newTimelinePin(const QJsonObject &pin);
    void delTimelinePin(const QString &guid);

protected:
    void timerEvent(QTimerEvent *event) override;

private:
    QOrganizerManager *m_manager;
    QHash<QString,QDateTime> m_track;
    bool m_disabled = false;
    quint32 m_windowStart = 2;
    quint32 m_windowEnd = 7;
};

#endif // ORGANIZERADAPTER_H
