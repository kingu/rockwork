#include "ubuntuplatform.h"

#include "callchannelobserver.h"
#include "organizeradapter.h"
#include "notificationmonitor.h"

#include <QDBusConnection>
#include <QDBusConnectionInterface>
#include <QDebug>

// qmenumodel
#include "dbus-enums.h"
#include "liblomiri-url-dispatcher/lomiri-url-dispatcher.h"

UbuntuPlatform::UbuntuPlatform(QObject *parent):
    PlatformInterface(parent)
{
    // canned response prereqs: telepathy message type and msg sources we can handle
    qDBusRegisterMetaType<QList<QVariantMap>>();
    m_cans.insert("SMS",QStringList());

    // Notifications
    m_notificationMonitor = new UbuntuNotifications::NotificationMonitor(this);
    m_postalNotifications = new PostalNotifications();
    connect(m_notificationMonitor, &UbuntuNotifications::NotificationMonitor::notification, this, &UbuntuPlatform::newNotificationPin);

    // Music
    m_musicController = new MusicController(this);
    connect(m_musicController, SIGNAL(metadataChanged()), SLOT(fetchMusicMetadata()));
    connect(m_musicController, SIGNAL(statusChanged()), SLOT(updateMusicStatus()));
    connect(m_musicController, SIGNAL(positionChanged()), SLOT(updateMusicStatus()));

    // Calls
    m_telepathyMonitor = new TelepathyMonitor(this);
    connect(m_telepathyMonitor, &TelepathyMonitor::incomingCall, this, &UbuntuPlatform::incomingCall);
    connect(m_telepathyMonitor, &TelepathyMonitor::callStarted, this, &UbuntuPlatform::callStarted);
    connect(m_telepathyMonitor, &TelepathyMonitor::callEnded, this, &UbuntuPlatform::callEnded);

    // Organizer
    m_organizerAdapter = new OrganizerAdapter(this);
    m_organizerAdapter->refresh();
    connect(m_organizerAdapter, &OrganizerAdapter::newTimelinePin, this, &PlatformInterface::newTimelinePin);
    connect(m_organizerAdapter, &OrganizerAdapter::delTimelinePin, this, &PlatformInterface::delTimelinePin);
}

UbuntuPlatform::~UbuntuPlatform()
{
    delete m_notificationMonitor;
    delete m_musicController;
    delete m_telepathyMonitor;
    delete m_organizerAdapter;
}

struct AppID {
    QString type;
    QString sender;
    QString appName;
};

AppID getAppID(UbuntuNotifications::Notification *notification)
{
    QString sender = notification->sender();
    AppID ret;
    if (sender.contains("twitter")) {
        ret.type="twitter";
    } else if (sender.contains("teleports")) {
        ret.type="telegram";
    } else if (sender.contains("dekko")) {
        ret.type="email";
    } else if (sender == "Telephony Service Indicator") {
        ret.type="sms";
        ret.sender="SMS";
        ret.appName="SMS";
        return ret;
    } else {
        ret.type="generic";
    }
    ret.sender = sender;
    ret.appName = notification->appName();
    return ret;
}


const QHash<QString,QStringList>& UbuntuPlatform::cannedResponses() const
{
    return m_cans;
}
void UbuntuPlatform::setCannedResponses(const QHash<QString, QStringList> &cans)
{
    // Accept only what we can handle, don't inject more than we have
    foreach(const QString &key, m_cans.keys()) {
        if(cans.contains(key))
            m_cans.insert(key,cans.value(key));
    }
}

void UbuntuPlatform::newNotificationPin(UbuntuNotifications::Notification *notification)
{
    qDebug() << "Got new notification from platform:" << notification->appName() << notification->summary();

    QJsonObject pin;
    QJsonObject layout;
    QJsonArray actions;

    AppID a = getAppID(notification);
    QStringList res = PlatformInterface::AppResMap.contains(a.type) ? PlatformInterface::AppResMap.value(a.type) : PlatformInterface::AppResMap.value("unknown");

    pin.insert("id",QString("%1.%2.%3").arg(a.sender).arg(notification->timestamp().toTime_t()).arg(notification->id()));
    QUuid guid = PlatformInterface::idToGuid(pin.value("id").toString());
    pin.insert("createTime", notification->timestamp().toUTC().toString(Qt::ISODate));
    pin.insert("guid",guid.toString().mid(1,36));
    pin.insert("type",QString("notification"));
    pin.insert("dataSource",QString("%1:%2").arg(a.sender).arg(PlatformInterface::SysID));
    pin.insert("appName",a.appName);
    pin.insert("sourceIcon",notification->icon());

    if(res.count()>2 && !res.at(2).isEmpty()) {
        pin.insert("sourceName",res.at(2));
    }

    // Dismiss action is added implicitly by TimelinePin class
    if(m_cans.contains(a.sender) && !m_cans.value(a.sender).isEmpty()) {
        // We should have responses only for something we could do
        QJsonObject response;
        response.insert("type",QString("response"));
        response.insert("title",QString("Response"));
        response.insert("cannedResponse",QJsonArray::fromStringList(m_cans.value(a.sender)));
        actions.append(response);
    }

    // Explicit open* will override implicit one
    foreach (const QString &actToken, notification->actions()) {
        if (actToken.contains(QRegExp("^[a-z]*://"))) {
            qDebug() << "found action" << actToken;
            QJsonObject action;
            action.insert("type",QString("open:%1").arg(actToken));
            action.insert("title",QString("Open on Phone"));
            actions.append(action);
            // Sender is sent back as part of canned message response
            layout.insert("sender",actToken);
            break;
        }
    }

    if (a.sender == "SMS") { // Hack for setting actToken manually for SMSes
        qDebug() << "setting token for SMS";
        QString token = notification->summary();
        if (token.contains("-")) {
            QString splitToken = "sms:" + token.split("-").at(0).simplified().remove(' ');
            layout.insert("sender",splitToken);
        } else {
            layout.insert("sender","sms:" + token);
        }
    }

    pin.insert("actions",actions);

    layout.insert("type",QString("commNotification"));

    if (a.type == "generic") {
        layout.insert("title",a.appName);
    }

    layout.insert("subtitle",notification->summary());
    layout.insert("body",notification->body());

    layout.insert("tinyIcon",res.at(0));
    layout.insert("backgroundColor",res.at(1));

    pin.insert("layout",layout);

    m_notifs.insert(guid, notification); // keep for the action. TimelineManager will take care cleaning it up

    qDebug() << "Emitting new pin" << pin.value("id").toString() << pin.value("dataSource").toString() << pin.value("guid").toString();
    emit newTimelinePin(pin);
}

void UbuntuPlatform::syncOrganizer(qint32 end) const
{
    m_organizerAdapter->reSync(end);
}

void UbuntuPlatform::stopOrganizer() const
{
    m_organizerAdapter->disable();
}

void UbuntuPlatform::sendMusicControlCommand(MusicControlButton controlButton)
{
    switch (controlButton) {
    case MusicControlPlayPause:
        m_musicController->playPause();
        break;
    case MusicControlPause:
        m_musicController->pause();
        break;
    case MusicControlPlay:
        m_musicController->play();
        break;
    case MusicControlPreviousTrack:
        m_musicController->previous();
        break;
    case MusicControlNextTrack:
        m_musicController->next();
        break;
    case MusicControlVolumeUp:
        m_musicController->volumeUp();
        break;
    case MusicControlVolumeDown:
        m_musicController->volumeDown();
        break;
    default:
        ;
    }
}

MusicMetaData UbuntuPlatform::musicMetaData() const
{
    return m_musicMetaData;
}

void UbuntuPlatform::hangupCall(uint cookie)
{
    m_telepathyMonitor->hangupCall(cookie);
}

void UbuntuPlatform::sendTextMessage(const QString &account, const QString &contact, const QString &text) const
{
    QString acct = account.isEmpty() ? "/ril_0" : account;
    QDBusInterface iface("org.ofono", acct, "org.ofono.MessageManager", QDBusConnection::systemBus());
    iface.call("SendMessage", contact, text);
}

void UbuntuPlatform::actionTriggered(const QUuid &uuid, const QString &actToken, const QJsonObject &param) const
{
    qDebug() << "Triggering notification " << uuid << " action " << actToken << QJsonDocument(param).toJson();;
    UbuntuNotifications::Notification *notif = m_notifs.value(uuid);
    QStringList splitActToken = actToken.split(":");
    if (notif) {
        if (splitActToken.first() == "open") {
            removeNotification(uuid);
            QString act(actToken);
            act.remove("open:");
            lomiri_url_dispatch_send(act.toStdString().c_str(), [] (const gchar *, gboolean, gpointer) {}, nullptr);
        } else if (actToken == "response") {
            if (param.contains("sender")) {
                QString sender(param.value("sender").toString());
                sender.remove("sms:");
                sendTextMessage("/ril_0", sender, param.value("title").toString());
            }
        }
    }
    else
        qDebug() << "Not found";

}

void UbuntuPlatform::removeNotification(const QUuid &uuid) const
{
    qDebug() << "Removing notification " << uuid;
    UbuntuNotifications::Notification *notif = m_notifs.value(uuid);
    if (notif) {
        notif->close();
        m_postalNotifications->closeNotification(notif->sender());
        m_notifs.remove(uuid);
    }
    else
        qDebug() << "Not found";
}

void UbuntuPlatform::fetchMusicMetadata()
{
    m_musicMetaData.artist = m_musicController->artist();
    m_musicMetaData.album = m_musicController->album();
    m_musicMetaData.title = m_musicController->title();
    m_musicMetaData.duration = m_musicController->duration();
    qDebug() << "New track " << m_musicMetaData.title << ". Length: " << m_musicMetaData.duration;
    emit musicMetadataChanged(m_musicMetaData);
    updateMusicStatus();
}

void UbuntuPlatform::updateMusicStatus() {
    MusicPlayState playState = getMusicPlayState();
    emit musicPlayStateChanged(playState);
}

MusicPlayState UbuntuPlatform::getMusicPlayState() const {
    MusicPlayState playState;
    switch (m_musicController->status()) {
        case MusicController::Status::StatusNoPlayer:
            playState.state = MusicPlayState::StateUnknown;
        break;
        case MusicController::Status::StatusStopped:
            playState.state = MusicPlayState::StatePaused;
        break;
        case MusicController::Status::StatusPaused:
            playState.state = MusicPlayState::StatePaused;
        break;
        case MusicController::Status::StatusPlaying:
            playState.state = MusicPlayState::StatePlaying;
        break;
        default:
            playState.state = MusicPlayState::StateUnknown;
    }
    switch (m_musicController->repeat()) {
        case MusicController::RepeatStatus::RepeatNone:
            playState.repeat = MusicPlayState::RepeatOff;
        break;
        case MusicController::RepeatStatus::RepeatTrack:
            playState.repeat = MusicPlayState::RepeatOne;
        break;
        case MusicController::RepeatStatus::RepeatPlaylist:
            playState.repeat = MusicPlayState::RepeatAll;
        break;
        default:
            playState.repeat = MusicPlayState::RepeatUnknown;
    }

    playState.trackPosition = m_musicController->position()/1000;
    if (m_musicController->shuffle())
        playState.shuffle = MusicPlayState::ShuffleOn;
    else
        playState.shuffle = MusicPlayState::ShuffleOff;

    return playState;
}

bool UbuntuPlatform::deviceIsActive() const
{
    return true; // HACK: implement using repowerd later
}
