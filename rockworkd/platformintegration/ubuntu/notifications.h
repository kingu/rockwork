/*
 *  libwatchfish - library with common functionality for SailfishOS smartwatch connector programs.
 *  Copyright (C) 2015 Javier S. Pedro <dev.git@javispedro.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WATCHFISH_NOTIFICATION_H
#define WATCHFISH_NOTIFICATION_H

#include <QtCore/QObject>
#include <QtCore/QDateTime>
#include <QtCore/QStringList>

namespace UbuntuNotifications {

class NotificationPrivate;

class Notification : public QObject
{
	Q_OBJECT
    Q_DECLARE_PRIVATE(Notification)

	/** Notification ID */
	Q_PROPERTY(uint id READ id CONSTANT)
	/** Name of sender program */
	Q_PROPERTY(QString sender READ sender WRITE setSender NOTIFY senderChanged)
	Q_PROPERTY(QString summary READ summary WRITE setSummary NOTIFY summaryChanged)
    Q_PROPERTY(QString body READ body WRITE setBody NOTIFY bodyChanged)
    Q_PROPERTY(QString appName READ appName WRITE setAppName NOTIFY appNameChanged)
	Q_PROPERTY(QDateTime timestamp READ timestamp WRITE setTimestamp NOTIFY timestampChanged)
	/** Icon file path */
	Q_PROPERTY(QString icon READ icon WRITE setIcon NOTIFY iconChanged)
	Q_PROPERTY(int urgency READ urgency WRITE setUrgency NOTIFY urgencyChanged)

	Q_PROPERTY(QStringList actions READ actions NOTIFY actionsChanged)

public:
    explicit Notification(uint id, QObject *parent = 0);
    ~Notification();

	uint id() const;

	QString sender() const;
	void setSender(const QString &sender);

	QString summary() const;
	void setSummary(const QString &summary);

	QString body() const;
	void setBody(const QString &body);

    QString appName() const;
    void setAppName(const QString &appName);

	QDateTime timestamp() const;
	void setTimestamp(const QDateTime &dt);

	QString icon() const;
	void setIcon(const QString &icon);

	int urgency() const;
	void setUrgency(int urgency);

	QStringList actions() const;
        void setActions(const QStringList &actions);

public slots:
	void close();

signals:
	void senderChanged();
	void summaryChanged();
    void bodyChanged();
    void appNameChanged();
	void timestampChanged();
	void iconChanged();
	void urgencyChanged();

	void actionsChanged();

private:
    NotificationPrivate * const d_ptr;
};

}
#endif // WATCHFISH_NOTIFICATION_H
