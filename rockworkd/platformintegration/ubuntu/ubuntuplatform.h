#ifndef UBUNTUPLATFORM_H
#define UBUNTUPLATFORM_H

#include "libpebble/platforminterface.h"
#include "libpebble/enums.h"

#include "postalnotifications.h"
#include "notificationmonitor.h"
#include "musiccontroller.h"

#include <QDBusInterface>
#include <TelepathyQt/AbstractClientObserver>

#include <qdbusactiongroup.h>
#include <qstateaction.h>

class QDBusPendingCallWatcher;
class TelepathyMonitor;
class OrganizerAdapter;

class UbuntuPlatform : public PlatformInterface, public QDBusContext
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.freedesktop.Notifications")


public:
    UbuntuPlatform(QObject *parent = 0);
    ~UbuntuPlatform();

    void sendMusicControlCommand(MusicControlButton controlButton) override;
    MusicMetaData musicMetaData() const override;
    void hangupCall(uint cookie) override;

    void syncOrganizer(qint32 end) const override;
    void stopOrganizer() const override;
    MusicPlayState getMusicPlayState() const override;

    void actionTriggered(const QUuid &uuid, const QString &actToken, const QJsonObject &param) const override;
    void removeNotification(const QUuid &uuid) const override;
    bool deviceIsActive() const override;

    const QHash<QString,QStringList>& cannedResponses() const override;
    void setCannedResponses(const QHash<QString, QStringList> &cans) override;
    void sendTextMessage(const QString &account, const QString &contact, const QString &text) const override;

public slots:
    //void onNotification(UbuntuNotifications::Notification *notification);
    void newNotificationPin(UbuntuNotifications::Notification *notification);
    void updateMusicStatus();

private slots:
    void fetchMusicMetadata();

private:
    MusicMetaData m_musicMetaData;

    TelepathyMonitor *m_telepathyMonitor;
    OrganizerAdapter *m_organizerAdapter;
    mutable QMap<QUuid, UbuntuNotifications::Notification*> m_notifs;
    UbuntuNotifications::NotificationMonitor *m_notificationMonitor;
    PostalNotifications *m_postalNotifications;
    MusicController *m_musicController;
    QTimer m_syncTimer;
    QHash<QString,QStringList> m_cans;
};

#endif // UBUNTUPLATFORM_H
