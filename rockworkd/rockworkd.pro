QT += core bluetooth dbus network contacts qml location organizer websockets positioning sql
QT -= gui

include(../version.pri)
load(ubuntu-click)

TARGET = rockworkd
CONFIG += c++11
#CONFIG -= app_bundle

TEMPLATE = app

#TODO: figure why pkgconfig doesn't work in the click chroot
#CONFIG += link_pkgconfig
#PKGCONFIG += url-dispatcher-1
INCLUDEPATH += /usr/lib/$$(ARCH_TRIPLET)/glib-2.0/include /usr/include/glib-2.0/
LIBS += -llomiri-url-dispatcher -llomiri-app-launch

INCLUDEPATH += /usr/include/telepathy-qt5/ /usr/include/qmenumodel/ /usr/include/dbus-1.0/ /usr/lib/$$(ARCH_TRIPLET)/dbus-1.0/include/ /usr/include/$$(ARCH_TRIPLET)/qt5/MprisQt/
INCLUDEPATH += /usr/include/liblomiri-app-launch-0/ /usr/include/mirclient/ /usr/include/mircore/
LIBS += -lquazip5 -ltelepathy-qt5 -lqmenumodel -ldbus-1 -lmpris-qt5

SOURCES += main.cpp \
    libpebble/watchconnection.cpp \
    libpebble/pebble.cpp \
    libpebble/watchdatareader.cpp \
    libpebble/watchdatawriter.cpp \
    libpebble/devconnection.cpp \
    libpebble/musicendpoint.cpp \
    libpebble/phonecallendpoint.cpp \
    libpebble/musicmetadata.cpp \
    libpebble/jskit/jskitmanager.cpp \
    libpebble/jskit/jskitconsole.cpp \
    libpebble/jskit/jskitgeolocation.cpp \
    libpebble/jskit/jskitlocalstorage.cpp \
    libpebble/jskit/jskitpebble.cpp \
    libpebble/jskit/jskitxmlhttprequest.cpp \
    libpebble/jskit/jskittimer.cpp \
    libpebble/jskit/jskitperformance.cpp \
    libpebble/jskit/jskitwebsocket.cpp \
    libpebble/appglances.cpp \
    libpebble/appinfo.cpp \
    libpebble/appmanager.cpp \
    libpebble/appmsgmanager.cpp \
    libpebble/uploadmanager.cpp \
    libpebble/weatherapp.cpp \
    libpebble/webweatherprovider.cpp \
    libpebble/weatherprovidertwc.cpp \
    libpebble/weatherproviderwu.cpp \
    libpebble/sendtextapp.cpp \
    libpebble/bluez/bluezclient.cpp \
    libpebble/bluez/bluez_agentmanager1.cpp \
    libpebble/bluez/bluez_adapter1.cpp \
    libpebble/bluez/bluez_device1.cpp \
    libpebble/bluez/freedesktop_objectmanager.cpp \
    libpebble/bluez/freedesktop_properties.cpp \
    core.cpp \
    pebblemanager.cpp \
    dbusinterface.cpp \
# Platform integration part
    platformintegration/ubuntu/ubuntuplatform.cpp \
    platformintegration/ubuntu/callchannelobserver.cpp \
    libpebble/blobdb.cpp \
    libpebble/timelineitem.cpp \
    libpebble/timelinemanager.cpp \
    libpebble/timelinesync.cpp \
    libpebble/webservices.cpp \
    platformintegration/ubuntu/organizeradapter.cpp \
    platformintegration/ubuntu/notificationmonitor.cpp \
    platformintegration/ubuntu/notifications.cpp \
    platformintegration/ubuntu/modecontrolentity.cpp \
    platformintegration/ubuntu/musiccontroller.cpp \
    platformintegration/ubuntu/lomiri-app-launch.cpp \
    platformintegration/ubuntu/postalnotifications.cpp \
    libpebble/appmetadata.cpp \
    libpebble/appdownloader.cpp \
    libpebble/screenshotendpoint.cpp \
    libpebble/firmwaredownloader.cpp \
    libpebble/bundle.cpp \
    libpebble/watchlogendpoint.cpp \
    libpebble/ziphelper.cpp \
    libpebble/healthdata.cpp \
    libpebble/healthparams.cpp \
    libpebble/dataloggingendpoint.cpp \
    libpebble/batterylevel.cpp \
    libpebble/pingpong.cpp \
    libpebble/voiceendpoint.cpp

HEADERS += \
    libpebble/watchconnection.h \
    libpebble/pebble.h \
    libpebble/watchdatareader.h \
    libpebble/watchdatawriter.h \
    libpebble/devconnection.h \
    libpebble/musicendpoint.h \
    libpebble/musicmetadata.h \
    libpebble/phonecallendpoint.h \
    libpebble/platforminterface.h \
    libpebble/jskit/jskitmanager.h \
    libpebble/jskit/jskitconsole.h \
    libpebble/jskit/jskitgeolocation.h \
    libpebble/jskit/jskitlocalstorage.h \
    libpebble/jskit/jskitpebble.h \
    libpebble/jskit/jskitxmlhttprequest.h \
    libpebble/jskit/jskittimer.h \
    libpebble/jskit/jskitperformance.h \
    libpebble/jskit/jskitwebsocket.h \
    libpebble/appglances.h \
    libpebble/appinfo.h \
    libpebble/appmanager.h \
    libpebble/appmsgmanager.h \
    libpebble/uploadmanager.h \
    libpebble/weatherapp.h \
    libpebble/webweatherprovider.h \
    libpebble/weatherprovidertwc.h \
    libpebble/weatherproviderwu.h \
    libpebble/sendtextapp.h \
    libpebble/bluez/bluezclient.h \
    libpebble/bluez/bluez_agentmanager1.h \
    libpebble/bluez/bluez_adapter1.h \
    libpebble/bluez/bluez_device1.h \
    libpebble/bluez/freedesktop_objectmanager.h \
    libpebble/bluez/freedesktop_properties.h \
    core.h \
    pebblemanager.h \
    dbusinterface.h \
# Platform integration part
    platformintegration/ubuntu/ubuntuplatform.h \
    platformintegration/ubuntu/callchannelobserver.h \
    libpebble/blobdb.h \
    libpebble/timelineitem.h \
    libpebble/timelinemanager.h \
    libpebble/timelinesync.h \
    libpebble/webservices.h \
    platformintegration/ubuntu/organizeradapter.h \
    platformintegration/ubuntu/notificationmonitor.h \
    platformintegration/ubuntu/notifications.h \
    platformintegration/ubuntu/modecontrolentity.h \
    platformintegration/ubuntu/nokia-mce-dbus-names.h \
    platformintegration/ubuntu/musiccontroller.h \
    platformintegration/ubuntu/lomiri-app-launch.h \
    platformintegration/ubuntu/postalnotifications.h \
    libpebble/appmetadata.h \
    libpebble/appdownloader.h \
    libpebble/enums.h \
    libpebble/screenshotendpoint.h \
    libpebble/firmwaredownloader.h \
    libpebble/bundle.h \
    libpebble/watchlogendpoint.h \
    libpebble/ziphelper.h \
    libpebble/healthdata.h \
    libpebble/healthparams.h \
    libpebble/dataloggingendpoint.h \
    libpebble/batterylevel.h \
    libpebble/pingpong.h \
    libpebble/voiceendpoint.h

testing: {
    SOURCES += platformintegration/testing/testingplatform.cpp
    HEADERS += platformintegration/testing/testingplatform.h
    RESOURCES += platformintegration/testing/testui.qrc
    DEFINES += ENABLE_TESTING
    QT += qml quick
}

libs.files = /usr/lib/$$(ARCH_TRIPLET)/libquazip5.so.1.0.0 \
             /usr/lib/$$(ARCH_TRIPLET)/libquazip5.so.1
libs.path = $${UBUNTU_CLICK_BINARY_PATH}/..
INSTALLS += libs


# Default rules for deployment.
target.path = $${UBUNTU_CLICK_BINARY_PATH}
INSTALLS+=target

SHARED_DATA_PATH = /opt/click.ubuntu.com/rockwork.mzanetti/current
#fetch from https://github.com/pebble/pypkjs/blob/master/pypkjs/timeline/layouts.json
# https://raw.githubusercontent.com/pebble/pypkjs/master/pypkjs/timeline/layouts.json

DISTFILES += JSON_FILES

QMAKE_POST_LINK = sed -i s/@VERSION@/$$VERSION/g $$OUT_PWD/../manifest.json || exit 0
#QMAKE_POST_LINK = echo $$OUT_PWD/../manifest.json > /tmp/huhu;

RESOURCES += \
    libpebble/jskit/jsfiles.qrc

DEFINES += 'SHARED_DATA_PATH=\\"$${SHARED_DATA_PATH}\\"'
